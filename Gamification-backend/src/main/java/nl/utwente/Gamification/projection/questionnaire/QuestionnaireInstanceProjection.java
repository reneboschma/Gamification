package nl.utwente.Gamification.projection.questionnaire;

import nl.utwente.Gamification.model.db.Question;

import java.util.List;

public interface QuestionnaireInstanceProjection {


    List<QuestionProjection> getQuestions();


}
