package nl.utwente.Gamification.projection.questionnaire;

public interface PermissionProjection {

    boolean getActivated();

    boolean getPermission();

    boolean getTesttype();

    boolean getTest();

    boolean getFile();


}
