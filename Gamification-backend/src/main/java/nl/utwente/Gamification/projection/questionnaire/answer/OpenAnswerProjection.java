package nl.utwente.Gamification.projection.questionnaire.answer;

public interface OpenAnswerProjection extends AnswerProjection{

    String getContent();

}
