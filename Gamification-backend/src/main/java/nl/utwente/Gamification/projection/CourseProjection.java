package nl.utwente.Gamification.projection;

public interface CourseProjection {

    long getId();
    String getCourseName();
    String getCourseCode();
    int getYear();

}
