package nl.utwente.Gamification.projection.questionnaire.answer;

public interface UploadAnswerProjection extends AnswerProjection {

    long getFileId();
    String getFileName();
}
