package nl.utwente.Gamification.projection.questionnaire;

import java.sql.Date;

public interface QuestionnaireIdProjection {

    Long getId();
    String getTestName();
    String getCourseName();
    Date getTestDate();

}
