package nl.utwente.Gamification.projection.questionnaire.answer;

public interface AnswerProjection {

    long getQuestionId();

}
