package nl.utwente.Gamification.projection.questionnaire.answer;

public interface ClosedAnswerProjection extends AnswerProjection {

    long getOptionId();

}
