package nl.utwente.Gamification.projection.questionnaire;

import nl.utwente.Gamification.model.db.Question;
import nl.utwente.Gamification.model.db.answer.Answer;

public interface QuestionProjection {

    Question getQuestion();
    Answer getAnswer();

}
