package nl.utwente.Gamification.model.db;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Permission {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy= GenerationType.AUTO)
    private String id;

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "teacherId")
//    private Teacher teacher;

    /**
     * Indicates whether a user is activated. A user must be activated to be able to do anything with the system.
     */
    private boolean activated = true;

    /**
     * Indicates the permission that one is allowed to change permissions of users
     */
    private boolean permission = true;

    /**
     * Indicates whether one is allowed to add or alter testtypes
     */
    private boolean testtype = true;


    /**
     * Indicates whether one is allowed to add or alter tests
     */
    private boolean test = true;

    /**
     * Indicates whether one is allowed to add or alter a questionnaire
     */
    private boolean questionnaire = true;


    /**
     * Indicates whether one is allowed to download files of other people
     */
    private boolean file = false;

    /**
     * Indicates whether one is allowed to add a new course
     */
    private boolean course = true;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public boolean isPermission() {
        return permission;
    }

    public void setPermission(boolean permission) {
        this.permission = permission;
    }

    public boolean isTesttype() {
        return testtype;
    }

    public void setTesttype(boolean testtype) {
        this.testtype = testtype;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public boolean isQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(boolean questionnaire) {
        this.questionnaire = questionnaire;
    }

    public boolean isFile() {
        return file;
    }

    public void setFile(boolean file) {
        this.file = file;
    }

    public boolean isCourse() {
        return course;
    }

    public void setCourse(boolean course) {
        this.course = course;
    }
}
