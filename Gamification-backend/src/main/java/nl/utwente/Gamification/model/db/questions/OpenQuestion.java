package nl.utwente.Gamification.model.db.questions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.utwente.Gamification.model.db.QuestionType;
import nl.utwente.Gamification.model.db.Question;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@JsonTypeName("OPEN")

@Entity
@DiscriminatorValue("OPEN")
public class OpenQuestion extends Question {

    public OpenQuestion() {
        this.type = QuestionType.OPEN;
    }
}
