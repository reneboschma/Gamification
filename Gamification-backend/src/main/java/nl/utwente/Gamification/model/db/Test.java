package nl.utwente.Gamification.model.db;


import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
public class Test {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    protected Long id;


    /**
     * The name of the test. For example, in the module Pearls of Informatics (the course) you have the test Functional Programming.
     */
    private String testName;

    /**
     * Indicates the type of the test. For example, if it is a written test.
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "questionType")
    private TestType type;

    /**
     * Indicates the day when the test took place.
     */
    private Date date;

    /**
     * Indicates the main responsible teacher for this test
     */
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="teacherId")
    private Teacher mainResponsible;


    /**
     * Indicates to which course this tests belongs
     */
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="courseId")
    private Course course;

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public TestType getType() {
        return type;
    }

    public void setType(TestType type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Teacher getMainResponsible() {
        return mainResponsible;
    }

    public void setMainResponsible(Teacher mainResponsible) {
        this.mainResponsible = mainResponsible;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
