package nl.utwente.Gamification.model.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class QuestionnaireInstance {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    protected Long id;

    /**
     * The test this questionnaire instance is assigned to
     */
    @Basic(optional = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="testId")
    private Test test;

    /**
     * The assigned teacher to fill in this questionnaire
     */
    @Basic(optional = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="teacherId")
    private Teacher teacher;

    /**
     * The questionnaire to be filled in
     */
    @Basic(optional = false)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="questionnaireId")
    private Questionnaire questionnaire;

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }
}
