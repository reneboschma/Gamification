package nl.utwente.Gamification.model.db.questions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.utwente.Gamification.model.db.QuestionType;
import nl.utwente.Gamification.model.db.Question;
import org.springframework.core.annotation.Order;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@JsonTypeName("CLOSED")
@Entity
@DiscriminatorValue("CLOSED")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ClosedQuestion extends Question {




    public ClosedQuestion() {
        this.type = QuestionType.CLOSED;
    }

    /**
     * The given options for this closed questions
     */
    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name="closedQuestionId")
    @OrderColumn
    private List<Option> options;

    /**
     * Explanation given by this question
     */
    @Column(columnDefinition="TEXT")
    private String explanation;

    public List<Option> getOptions() {
        return options;
    }

    public String getExplanation() {
        return explanation;
    }

    public void setExplanation(String explanation) {
        this.explanation = explanation;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }
}
