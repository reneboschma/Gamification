package nl.utwente.Gamification.model.db.answer;

import java.io.Serializable;
import java.util.Objects;

public class AnswerId implements Serializable{

    long questionnaireInstanceId;

    long questionId;

    public AnswerId() {
    }

    public AnswerId(long questionnaireInstanceId, long questionId) {

        this.questionnaireInstanceId = questionnaireInstanceId;
        this.questionId = questionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnswerId answerId = (AnswerId) o;
        return questionnaireInstanceId == answerId.questionnaireInstanceId &&
                questionId == answerId.questionId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(questionnaireInstanceId, questionId);
    }

    public long getQuestionnaireInstanceId() {
        return questionnaireInstanceId;
    }

    public void setQuestionnaireInstanceId(long questionnaireInstanceId) {
        this.questionnaireInstanceId = questionnaireInstanceId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
}
