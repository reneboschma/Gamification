package nl.utwente.Gamification.model.bean;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;
import java.util.List;

public class TestBean {


    private String testName;

    private long courseId;

    private long testTypeId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date date;

    private String mainResponsibleId;

    private List<String> teachersToFillInIds;


    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public long getTestTypeId() {
        return testTypeId;
    }

    public void setTestTypeId(long testTypeId) {
        this.testTypeId = testTypeId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }



    public long getCourseId() {
        return courseId;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public String getMainResponsibleId() {
        return mainResponsibleId;
    }

    public void setMainResponsibleId(String mainResponsibleId) {
        this.mainResponsibleId = mainResponsibleId;
    }

    public List<String> getTeachersToFillInIds() {
        return teachersToFillInIds;
    }

    public void setTeachersToFillInIds(List<String> teachersToFillInIds) {
        this.teachersToFillInIds = teachersToFillInIds;
    }
}
