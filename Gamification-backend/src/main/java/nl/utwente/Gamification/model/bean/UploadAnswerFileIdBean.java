package nl.utwente.Gamification.model.bean;

public class UploadAnswerFileIdBean {

    private long fileId;

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }
}
