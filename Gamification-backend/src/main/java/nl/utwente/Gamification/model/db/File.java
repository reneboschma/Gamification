package nl.utwente.Gamification.model.db;

import javax.persistence.*;

@Entity
public class File {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String fileURI;

    private String fileName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="ownerId")
    private Teacher owner;

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileURI() {
        return fileURI;
    }

    public Teacher getOwner() {
        return owner;
    }

    public void setOwner(Teacher owner) {
        this.owner = owner;
    }

    public void setFileURI(String fileURI) {
        this.fileURI = fileURI;
    }
}
