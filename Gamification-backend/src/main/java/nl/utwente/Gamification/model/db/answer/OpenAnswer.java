package nl.utwente.Gamification.model.db.answer;

import javax.persistence.*;

@Entity
@DiscriminatorValue("OPEN")
public class OpenAnswer extends Answer {

    @Column(columnDefinition="TEXT")
    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
