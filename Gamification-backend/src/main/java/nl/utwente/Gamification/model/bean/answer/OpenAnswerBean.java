package nl.utwente.Gamification.model.bean.answer;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("OPEN")
public class OpenAnswerBean extends AnswerBean{

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
