package nl.utwente.Gamification.model.bean;


import javax.validation.constraints.Size;

public class TestTypeBean {

    @Size(max = 10)
    private String shortname;

    private String description;

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortname) {
        this.shortname = shortname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
