package nl.utwente.Gamification.model.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import nl.utwente.Gamification.model.bean.answer.ClosedAnswerBean;
import nl.utwente.Gamification.model.bean.answer.OpenAnswerBean;
import nl.utwente.Gamification.model.bean.answer.UploadAnswerBean;
import nl.utwente.Gamification.model.db.questions.ClosedQuestion;
import nl.utwente.Gamification.model.db.questions.OpenQuestion;
import nl.utwente.Gamification.model.db.questions.UploadQuestion;

import javax.persistence.*;


@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ClosedQuestion.class, name = "CLOSED"),
        @JsonSubTypes.Type(value = UploadQuestion.class, name = "UPLOAD"),
        @JsonSubTypes.Type(value = OpenQuestion.class, name = "OPEN")
})
@Entity
@Inheritance
@DiscriminatorColumn(name="questionTypeInternal")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Question {


    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    protected Long id;

    /**
     * The statement of the question
     */
    protected String statement;

    /**
     * Indicates the type of the question.
     */
    protected QuestionType type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }
}
