package nl.utwente.Gamification.model.bean;

public class CourseBean {

    private String courseName;

    private String courseCode;

    private int year;

    private String courseCoordinatorId;

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCourseCoordinatorId() {
        return courseCoordinatorId;
    }

    public void setCourseCoordinatorId(String courseCoordinatorId) {
        this.courseCoordinatorId = courseCoordinatorId;
    }
}
