package nl.utwente.Gamification.model.db.questions;

import com.fasterxml.jackson.annotation.JsonTypeName;
import nl.utwente.Gamification.model.db.QuestionType;
import nl.utwente.Gamification.model.db.Question;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@JsonTypeName("UPLOAD")
@Entity
@DiscriminatorValue("UPLOAD")
public class UploadQuestion extends Question {

    public UploadQuestion(){
        this.type = QuestionType.UPLOAD;
    }
}
