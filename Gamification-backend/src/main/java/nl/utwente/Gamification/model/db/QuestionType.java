package nl.utwente.Gamification.model.db;

import nl.utwente.Gamification.model.bean.answer.AnswerBean;
import nl.utwente.Gamification.model.bean.answer.ClosedAnswerBean;
import nl.utwente.Gamification.model.bean.answer.OpenAnswerBean;
import nl.utwente.Gamification.model.bean.answer.UploadAnswerBean;
import nl.utwente.Gamification.model.db.answer.Answer;

public enum QuestionType {

    OPEN, CLOSED, UPLOAD;

    public boolean matchesAnswerBean(AnswerBean answer){
        return (answer instanceof OpenAnswerBean && this == OPEN)
                || (answer instanceof ClosedAnswerBean && this == CLOSED)
                || (answer instanceof UploadAnswerBean && this == UPLOAD);
    }

}
