package nl.utwente.Gamification.model.bean.answer;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("CLOSED")
public class ClosedAnswerBean extends AnswerBean{

    private long optionId;

    public long getOptionId() {
        return optionId;
    }

    public void setOptionId(long optionId) {
        this.optionId = optionId;
    }
}
