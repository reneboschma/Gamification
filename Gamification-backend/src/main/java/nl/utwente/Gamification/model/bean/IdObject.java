package nl.utwente.Gamification.model.bean;

public class IdObject {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
