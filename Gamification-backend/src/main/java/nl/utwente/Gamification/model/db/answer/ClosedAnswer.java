package nl.utwente.Gamification.model.db.answer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nl.utwente.Gamification.model.db.questions.Option;

import javax.persistence.*;

@Entity
@DiscriminatorValue("CLOSED")
public class ClosedAnswer extends Answer{

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "optionId")
    private Option option;

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }
}
