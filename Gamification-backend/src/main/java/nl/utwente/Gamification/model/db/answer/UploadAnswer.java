package nl.utwente.Gamification.model.db.answer;

import nl.utwente.Gamification.model.db.File;

import javax.persistence.*;

@Entity
@DiscriminatorValue("UPLOAD")
public class UploadAnswer extends Answer {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fileId")
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
