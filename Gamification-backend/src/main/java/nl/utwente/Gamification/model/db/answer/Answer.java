package nl.utwente.Gamification.model.db.answer;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@IdClass(AnswerId.class)
@Inheritance
@DiscriminatorColumn(name="answerTypeInternal")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Answer implements Serializable{

    @Id
    private long questionnaireInstanceId;

    @Id
    private long questionId;


    public long getQuestionnaireInstanceId() {
        return questionnaireInstanceId;
    }

    public void setQuestionnaireInstanceId(long questionnaireInstanceId) {
        this.questionnaireInstanceId = questionnaireInstanceId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
}
