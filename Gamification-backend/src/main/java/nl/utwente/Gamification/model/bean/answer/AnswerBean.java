package nl.utwente.Gamification.model.bean.answer;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModel;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ClosedAnswerBean.class, name = "CLOSED"),
        @JsonSubTypes.Type(value = UploadAnswerBean.class, name = "UPLOAD"),
        @JsonSubTypes.Type(value = OpenAnswerBean.class, name = "OPEN")
})
@ApiModel(value = "AnswerBean", subTypes = {UploadAnswerBean.class, ClosedAnswerBean.class, OpenAnswerBean.class}, discriminator = "type")
public class AnswerBean {

    private long questionnaireInstanceId;

    private long questionId;

    public long getQuestionnaireInstanceId() {
        return questionnaireInstanceId;
    }

    public void setQuestionnaireInstanceId(long questionnaireInstanceId) {
        this.questionnaireInstanceId = questionnaireInstanceId;
    }

    public long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(long questionId) {
        this.questionId = questionId;
    }
}
