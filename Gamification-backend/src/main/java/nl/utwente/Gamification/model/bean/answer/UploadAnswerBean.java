package nl.utwente.Gamification.model.bean.answer;

import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("UPLOAD")
public class UploadAnswerBean extends AnswerBean{

    private long fileId;

    public long getFileId() {
        return fileId;
    }

    public void setFileId(long fileId) {
        this.fileId = fileId;
    }
}
