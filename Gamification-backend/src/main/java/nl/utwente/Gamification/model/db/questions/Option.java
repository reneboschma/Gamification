package nl.utwente.Gamification.model.db.questions;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import nl.utwente.Gamification.model.db.Question;
import org.hibernate.annotations.Type;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Option {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private long id;

    /**
     * Describes the value of the option
     */
    private String option;

    /**
     * The optional follow-up question this option entails
     */
    @Basic(optional = true)
    @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    @JoinColumn(name="followUpQuestionId")
    private Question followUpQuestion;

    /**
     * Indicates whether the given option is a desired option, default value is true
     */

    @Column(columnDefinition = "TINYINT")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    private boolean desired = true;

    public boolean isDesired() {
        return desired;
    }

    public void setDesired(boolean desired) {
        this.desired = desired;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public Question getFollowUpQuestion() {
        return followUpQuestion;
    }

    public void setFollowUpQuestion(Question followUpQuestion) {
        this.followUpQuestion = followUpQuestion;
    }
}
