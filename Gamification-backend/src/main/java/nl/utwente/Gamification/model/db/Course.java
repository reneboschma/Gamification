package nl.utwente.Gamification.model.db;

import javax.persistence.*;
import java.util.List;

@Entity
public class Course {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    /**
     * The name of the course
     */
    private String courseName;

    /**
     * The course code, as used in Osiris, of the course
     */
    private String courseCode;

    /**
     * The year this course takes place. For instance Data and Informatics 2017/2018 would get 2017 as year.
     */
    private int year;

    /**
     * The course coordinator. In other words the main responsible of the course. This person needs to add all the tests
     */
    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="teacherId")
    private Teacher courseCoordinator;


    /**
     * The tests that are assigned to this test.
     */
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "courseId")
    private List<Test> tests;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Teacher getCourseCoordinator() {
        return courseCoordinator;
    }

    public void setCourseCoordinator(Teacher courseCoordinator) {
        this.courseCoordinator = courseCoordinator;
    }

    public List<Test> getTests() {
        return tests;
    }

    public void setTests(List<Test> tests) {
        this.tests = tests;
    }
}
