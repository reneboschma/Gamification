package nl.utwente.Gamification.model.bean;

import nl.utwente.Gamification.model.db.Question;

import java.util.List;

public class QuestionnaireBean {

    private String name;

    private long testTypeId;

    private List<Question> questions;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTestTypeId() {
        return testTypeId;
    }

    public void setTestTypeId(long testTypeId) {
        this.testTypeId = testTypeId;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }
}
