package nl.utwente.Gamification.model.db;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Teacher {

    /**
     * External id gotten from the service provider, in this case the m number
     */
    @Id
    private String id;

    /**
     * Main email of the user
     */
    private String mainEmail;


    /**
     * Secondary emails associated to this teacher
     */
    @ElementCollection(targetClass = String.class)
    private List<String> secondaryEmails;

    @OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="permissionId")
    private Permission permission = new Permission();


    public Teacher() { }

    public Teacher(String id, String mainEmail) {
        this.id = id;
        this.mainEmail = mainEmail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMainEmail() {
        return mainEmail;
    }

    public void setMainEmail(String mainEmail) {
        this.mainEmail = mainEmail;
    }

    public List<String> getSecondaryEmails() {
        return secondaryEmails;
    }

    public void setSecondaryEmails(List<String> secondaryEmails) {
        this.secondaryEmails = secondaryEmails;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }
}
