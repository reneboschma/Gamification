package nl.utwente.Gamification.controller;

import nl.utwente.Gamification.Constants;
import nl.utwente.Gamification.model.db.Teacher;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;

import javax.servlet.http.HttpSession;

@Controller
public class IndexController {

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private AuthenticateService authenticateService;

    @Value("${server.jit}")
    private boolean useJit;

    @RequestMapping("/")
    public String index(HttpSession session) {
        String email;
        String uid;
        Teacher teacher;

        try {
            SAMLCredential credential = (SAMLCredential) SecurityContextHolder.getContext().getAuthentication().getCredentials();
            uid = credential.getAttributeAsString("urn:mace:dir:attribute-def:uid");
            email = credential.getAttributeAsString("urn:mace:dir:attribute-def:mail");
            teacher = teacherService.addTeacher(uid, email);
        } catch (ClassCastException e){
            System.out.println("Could not construct SAMLCredential");
            email = "reneboschma@hotmail.com";
            uid = "reneboschma@hotmail.com";
            teacher = teacherService.addTeacher(uid, email);
        }

        session.setAttribute(Constants.UID, uid);
        authenticateService.setPermissionDataSession(session, teacher);

        System.out.println(String.format("---------USER LOGIN----------\nuid:%s\nmail:%s", uid, email));

        if (useJit) {
            String JSESSIONID = RequestContextHolder.currentRequestAttributes().getSessionId();
            return "redirect:http://localhost:4200/?session=" + JSESSIONID;
        } else {
            return "index";
        }
    }


}