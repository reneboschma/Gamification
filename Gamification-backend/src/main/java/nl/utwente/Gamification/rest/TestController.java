package nl.utwente.Gamification.rest;

import io.swagger.annotations.ApiOperation;
import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.TestBean;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

import static nl.utwente.Gamification.Constants.TEST;

@RestController
@RequestMapping("/rest/test")
public class TestController {

    @Autowired
    private AuthenticateService authenticateService;

    @Autowired
    private TestService testService;

    @ApiOperation(value = "Adds a new Test",
            notes = "Adds a new test to the database. Furthermore, a questionnaire instance will be added for all teachers " +
                    "specified in teachersToFillIn field. Whenever a teacher has not logged into this system, an account " +
                    "be created. When the teachersToFIllIn is empty, an exception is returned, as for every test at least " +
                    "one person needs to fill in a questionnaire. Furthemore, the mainResponsible must be known in the system, " +
                    "otherwise as well an exception is thrown. ")
    @RequestMapping(method = RequestMethod.POST)
    public void addTest(HttpSession session, @RequestBody TestBean testBean) throws NotAuthorizedException, BadRequestException, NotFoundException {
        authenticateService.authenticate(session, TEST);
        testService.addTest(testBean);
    }

}
