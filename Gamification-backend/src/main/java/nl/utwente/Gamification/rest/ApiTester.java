package nl.utwente.Gamification.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiTester {

    @RequestMapping("/rest/test")
    public String test(){
        return "Hello from backend";
    }


}
