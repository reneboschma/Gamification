package nl.utwente.Gamification.rest;

import io.swagger.annotations.ApiOperation;
import nl.utwente.Gamification.Constants;
import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.EmailBean;
import nl.utwente.Gamification.model.bean.TeacherBean;
import nl.utwente.Gamification.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController()
@RequestMapping("/rest/teacher")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    @ApiOperation(value = "Retrieves teacher information",
            notes = "Retrieves the information about the current logged in teacher")
    @RequestMapping(method = RequestMethod.GET)
    public TeacherBean getLoggedInInformation(HttpSession session) throws NotFoundException {
        return teacherService.getTeacherInformation((String) session.getAttribute(Constants.UID));
    }

    @ApiOperation(value = "Add secondary email",
            notes = "Adds secondary email of the current logged in teacher")
    @RequestMapping(method = RequestMethod.PUT)
    public void addSecondaryEmail(HttpSession session, @RequestBody EmailBean emailBean) throws NotFoundException, BadRequestException {
        String id = (String) session.getAttribute(Constants.UID);
        teacherService.addSecondaryEmail(id, emailBean.getEmail());
    }

    @ApiOperation(value = "Removes secondary email",
            notes = "Removes secondary email of the current logged in teacher")
    @RequestMapping(method = RequestMethod.DELETE)
    public void removeSecondaryEmail(HttpSession session, @RequestBody EmailBean emailBean) throws NotFoundException, BadRequestException {
        String id = (String) session.getAttribute(Constants.UID);
        teacherService.removeSecondaryEmail(id, emailBean.getEmail());
    }

    @ApiOperation(value = "Sets main email",
            notes = "Sets the main email of the current logged in teacher")
    @RequestMapping(method = RequestMethod.POST)
    public void setMainEmail(HttpSession session, @RequestBody EmailBean emailBean) throws NotFoundException, BadRequestException {
        String id = (String) session.getAttribute(Constants.UID);
        teacherService.setPrimaryEmail(id, emailBean.getEmail());
    }

}
