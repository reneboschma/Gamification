package nl.utwente.Gamification.rest.questionnaire;

import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.QuestionnaireBean;
import nl.utwente.Gamification.model.db.Questionnaire;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.QuestionnaireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

import static nl.utwente.Gamification.Constants.QUESTIONNAIRE;

@RestController
@RequestMapping("/rest/questionnaire/admin")
public class QuestionnaireController {

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private AuthenticateService authenticateService;

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public void addNewQuestionnaire(HttpSession session, @RequestBody QuestionnaireBean questionnaire) throws NotAuthorizedException, NotFoundException {
        //TODO check if is allowed to add
        authenticateService.authenticate(session, QUESTIONNAIRE);
        questionnaireService.addNewQuestionnaire(questionnaire);
    }
}
