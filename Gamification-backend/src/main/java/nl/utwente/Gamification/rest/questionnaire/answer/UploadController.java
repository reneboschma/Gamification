package nl.utwente.Gamification.rest.questionnaire.answer;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.utwente.Gamification.Constants;
import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.UploadAnswerFileIdBean;
import nl.utwente.Gamification.model.db.File;
import nl.utwente.Gamification.model.db.Teacher;
import nl.utwente.Gamification.repository.FileRepository;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.FileService;
import nl.utwente.Gamification.service.TeacherService;
import org.apache.tomcat.util.bcel.Const;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class UploadController {

    @Autowired
    private FileRepository fileRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private AuthenticateService authenticateService;

    @Autowired
    private ServletContext context;

    @Autowired
    private TeacherService teacherService;

    @ApiOperation(value = "Uploads a file",
            notes = "Saves the given file to the server. The only restriction on the file is that it needs to be under " +
                    "1GB. The server will respond with an UploadAnswerFileIdBean, This contains the id that can be used " +
                    "to re-access the file again.")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Given file does not follow the specified restrictions"),
            @ApiResponse(code = 404, message = "User is not yet added as user to the system")
    })
    @RequestMapping(method = RequestMethod.POST, value = "/rest/upload")
    public UploadAnswerFileIdBean uploadFile(HttpSession session, @RequestParam("file") MultipartFile uploadFile) throws BadRequestException, NotFoundException {
        if (uploadFile.isEmpty()) {
            throw new BadRequestException("Specified file may not be empty.");
        }
        //Retrieve User object
        String uid = (String) session.getAttribute(Constants.UID);
        Teacher teacher = teacherService.getTeacherById(uid);

        try {
            //First check if upload folder is present, if not create it
            Path parentDir = Paths.get(Constants.UPLOAD_PATH);
            if (!Files.exists(parentDir))
                Files.createDirectories(parentDir);

            //Check size is not over size limit
            if (uploadFile.getSize() > Constants.UPLOAD_SIZE_LIMIT) {
                throw new BadRequestException("Given file is to big.");
            }

            byte[] bytes = uploadFile.getBytes();
            String fileName = System.currentTimeMillis() + "-" + uploadFile.getOriginalFilename();
            String filePath = Constants.UPLOAD_PATH + fileName;
            Path path = Paths.get(filePath);
            Files.write(path, bytes);


            File file = new File();
            file.setFileURI(filePath);
            file.setOwner(teacher);
            file.setFileName(uploadFile.getOriginalFilename());
            fileRepository.save(file);

            UploadAnswerFileIdBean uploadBean = new UploadAnswerFileIdBean();
            uploadBean.setFileId(file.getId());
            return uploadBean;
        } catch (IOException e) {
            throw new BadRequestException("Could not save given file to the server.");
        }
    }


    @ApiOperation(value = "Downloads a file",
            notes = "Downloads the specified file from the server. The user must be authorized to download the file. ")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong downloading the file from the server"),
            @ApiResponse(code = 404, message = "The specified file could not be found"),
            @ApiResponse(code = 403, message = "User is not allowed to download this file")
    })
    @RequestMapping(value = "/rest/download/{fileId}", method = RequestMethod.GET)
    public void getFile(HttpSession session, @PathVariable("fileId") long fileId, HttpServletResponse response) throws NotFoundException, NotAuthorizedException, BadRequestException {
        File f = fileService.getFileById(fileId);
        authenticateService.authenticate(session, f.getOwner().getId(), "You are not allowed to download this file");

        try {
            java.io.File file = new java.io.File(f.getFileURI());

            if (file.exists()) {
                String mimeType = context.getMimeType(file.getPath());

                if (mimeType == null) {
                    mimeType = "application/octet-stream";
                }

                response.setContentType(mimeType);
                response.addHeader("Content-Disposition", "attachment; filename=" + file.getName());
                response.setContentLength((int) file.length());

                OutputStream os = response.getOutputStream();
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[4096];
                int b = -1;

                while ((b = fis.read(buffer)) != -1) {
                    os.write(buffer, 0, b);
                }

                fis.close();
                os.close();
            }

        } catch (IOException e) {
            throw new BadRequestException("Something went wrong downloading the file");
        }

    }






}
