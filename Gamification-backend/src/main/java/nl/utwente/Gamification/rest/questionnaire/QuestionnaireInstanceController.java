package nl.utwente.Gamification.rest.questionnaire;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.utwente.Gamification.Constants;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.IdObject;
import nl.utwente.Gamification.model.db.Questionnaire;
import nl.utwente.Gamification.model.db.QuestionnaireInstance;
import nl.utwente.Gamification.projection.questionnaire.QuestionnaireIdProjection;
import nl.utwente.Gamification.service.QuestionnaireGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.websocket.server.PathParam;
import java.util.List;

@RestController()
@RequestMapping("/rest/questionnaire")
public class QuestionnaireInstanceController {

    @Autowired
    private QuestionnaireGetService questionnaireGetService;

    @ApiOperation(value = "Retrieves open questionnaires",
            notes = "Retrieves all the open questionnaire instances of the current user.")
    @RequestMapping(method = RequestMethod.GET, value = "/list")
    public List<QuestionnaireIdProjection> getQuestionnairesToFillInOverview(HttpSession session) {
        String uid = (String) session.getAttribute(Constants.UID);
        return questionnaireGetService.getQuestionnairesToFillIn(uid);
    }

    @ApiOperation(value = "Retrieves the questionnaire",
            notes = "Retrieves the questionnaire instance identified by the given id and returns the corresponding questionnaire. This is the id of the questionnaire instance")
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Questionnaire instance does not belong to current user"),
            @ApiResponse(code = 404, message = "Questionnaire instance not found")
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Questionnaire getQuestionnaireFromQuestionnaireInstance(HttpSession session,
                                                                   @ApiParam(value = "id of a questionnaire instance")
                                                                   @PathVariable("id") long id) throws NotAuthorizedException, NotFoundException {
        String uid = (String) session.getAttribute(Constants.UID);
        QuestionnaireInstance instance = questionnaireGetService.getQuestionnaireInstance(id);
        if (!instance.getTeacher().getId().equals(uid)){
            throw new NotAuthorizedException("The given questionnaire is not yours to fill in.");
        }
        return instance.getQuestionnaire();
    }
}
