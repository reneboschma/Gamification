package nl.utwente.Gamification.rest;

import io.swagger.annotations.ApiOperation;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.TestTypeBean;
import nl.utwente.Gamification.model.db.TestType;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.TestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;

import static nl.utwente.Gamification.Constants.TESTTYPE;

@RestController()
@RequestMapping("/rest/testtype")
public class TestTypeController {

    @Autowired
    private TestTypeService testTypeService;

    @Autowired
    private AuthenticateService authenticateService;

    @ApiOperation(value = "Retrieves all TestTypes",
            notes = "Retrieves all the TestTypes in the database.")
    @RequestMapping(method = RequestMethod.GET)
    public List<TestType> getTestTypes(HttpSession session) throws NotAuthorizedException {
        authenticateService.authenticate(session);
        return testTypeService.getTestTypes();
    }

    @ApiOperation(value = "Retrieves a single TestType",
            notes = "Retrieves a single TestType")
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public TestType getTestType(HttpSession session, @PathVariable("id") long id) throws NotAuthorizedException, NotFoundException {
        authenticateService.authenticate(session);
        return testTypeService.getTestType(id);
    }

    @ApiOperation(value = "Adds a new TestType",
            notes = "Adds a new TestType to the database.")
    @RequestMapping(method = RequestMethod.POST)
    public void addTestType(HttpSession session, @RequestBody TestTypeBean testTypeBean) throws NotAuthorizedException {
        authenticateService.authenticate(session, TESTTYPE);
        testTypeService.addTestType(testTypeBean);
    }

    @ApiOperation(value = "Updates an existing TestType",
            notes = "Updates an existing TestType.")
    @RequestMapping(method = RequestMethod.PUT)
    public void updateTestType(HttpSession session, @RequestBody TestType testType) throws NotAuthorizedException, NotFoundException {
        authenticateService.authenticate(session, TESTTYPE);
        testTypeService.updateTestType(testType);
    }

}
