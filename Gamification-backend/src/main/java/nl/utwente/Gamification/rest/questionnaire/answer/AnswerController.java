package nl.utwente.Gamification.rest.questionnaire.answer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.answer.AnswerBean;
import nl.utwente.Gamification.model.bean.answer.ClosedAnswerBean;
import nl.utwente.Gamification.model.bean.answer.OpenAnswerBean;
import nl.utwente.Gamification.model.bean.answer.UploadAnswerBean;
import nl.utwente.Gamification.model.db.answer.Answer;
import nl.utwente.Gamification.projection.questionnaire.answer.AnswerProjection;
import nl.utwente.Gamification.service.AnswerService;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.QuestionnaireGetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/rest/answer")
public class AnswerController {

    @Autowired
    private AnswerService answerService;

    @Autowired
    private QuestionnaireGetService questionnaireGetService;

    @Autowired
    private AuthenticateService authenticateService;

    @ApiOperation(value = "Adds given answers",
            notes = "Adds the answer to the given questionnaire and question. The body must always contain a field type " +
                    "with values either: OPEN, CLOSED or UPLOAD to correctly handle the inheritance. The CLOSED answer " +
                    "contains an extra field optionId which contains the string value of the corresponding option id, " +
                    "the OPEN answer contains an extra field content which contains the string contents of the answer," +
                    "the UPLOAD answer contains an extra field of type long containing the file id. \n\n" +
                    "Furthermore it is needed that for a nested question it is important that the top level question is " +
                    "the first element in the list and the leaf question the last element in the list (intermediate questions " +
                    "must also be listed in order). If this is not done, just answered questions will be removed. It is also allowed " +
                    "to answer multiple nested questions at once, but this is not adviced. ")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "List of answers does not meet the requirements"),
            @ApiResponse(code = 403, message = "Questionnaire instance does not belong to current user"),
            @ApiResponse(code = 404, message = "Questionnaire instance not found")
    })
    @RequestMapping(method = RequestMethod.POST)
    public void addAnswer(HttpSession session, @RequestBody List<AnswerBean> answers) throws NotFoundException, NotAuthorizedException, BadRequestException {
        //Check if request contains at least one answer
        if(answers.size()<=0){
            throw new BadRequestException("At least one answer must be given.");
        }

        //Check if all questions are answered by the correct person
        for(AnswerBean answer : answers) {
            authenticateService.authenticate(session,
                    questionnaireGetService.getQuestionnaireInstance(answer.getQuestionnaireInstanceId()).getTeacher().getId(),
                    "The given questionnaire is not yours to answer.");
        }

        //Add answers
        answerService.addAnswer(answers);
    }

    @ApiOperation(value = "Retrieves all given answers of a questionnaireInstance",
            notes = "Returns all given answers of a questionnaireInstance. ")
    @ApiResponses(value = {
            @ApiResponse(code = 403, message = "Questionnaire instance does not belong to current user"),
            @ApiResponse(code = 404, message = "Questionnaire instance not found")
    })
    @RequestMapping(value = "/{questionnaireId}", method = RequestMethod.GET)
    public Set<AnswerProjection> getAnswersOfQuestionnaire(HttpSession session, @PathVariable("questionnaireId") long questionnaireId) throws NotFoundException, NotAuthorizedException {
        authenticateService.authenticate(session,
                questionnaireGetService.getQuestionnaireInstance(questionnaireId).getTeacher().getId(),
                "The given questionnaire is not yours to answer.");
        return answerService.getAnswers(questionnaireId);
    }

}
