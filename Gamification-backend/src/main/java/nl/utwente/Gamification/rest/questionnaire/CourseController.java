package nl.utwente.Gamification.rest.questionnaire;

import io.swagger.annotations.ApiOperation;
import nl.utwente.Gamification.Constants;
import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.CourseBean;
import nl.utwente.Gamification.model.bean.CourseEditBean;
import nl.utwente.Gamification.model.db.Course;
import nl.utwente.Gamification.projection.CourseProjection;
import nl.utwente.Gamification.service.AuthenticateService;
import nl.utwente.Gamification.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

import static nl.utwente.Gamification.Constants.COURSE;

@RestController
@RequestMapping("/rest/course")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @Autowired
    private AuthenticateService authenticateService;


    @ApiOperation(value = "Retrieves all courses",
            notes = "Retrieves all courses.")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public List<CourseProjection> getAllCourses(HttpSession session) throws NotAuthorizedException {
        authenticateService.authenticate(session);
        return courseService.getAllCourses();
    }

    @ApiOperation(value = "Retrieves all own courses",
            notes = "Retrieves all courses where the current logged in teacher is the coordinator of.")
    @RequestMapping(method = RequestMethod.GET, value = "/own")
    public List<CourseProjection> getOwnCourses(HttpSession session) throws NotAuthorizedException {
        authenticateService.authenticate(session);
        String uid = (String) session.getAttribute(Constants.UID);
        return courseService.getAllOwnCourses(uid);
    }

    @ApiOperation(value = "Adds a new Course",
            notes = "Adds a new course. The supplied teacher must be known in the system in order to assign a course to " +
                    "that person.")
    @RequestMapping(method = RequestMethod.POST)
    public void addCourse(HttpSession session, @RequestBody CourseBean bean) throws NotAuthorizedException, NotFoundException, BadRequestException {
        authenticateService.authenticate(session, COURSE);
        courseService.addCourse(bean);
    }

    @ApiOperation(value = "Edits a course",
            notes = "Edits a course. All fields supplied are updated. Furthermore, the person needs to have the course " +
                    "permission or needs to be the coordinator to edit the course. ")
    @RequestMapping(method = RequestMethod.PUT)
    public void editCourse(HttpSession session, @RequestBody CourseEditBean bean) throws NotFoundException, NotAuthorizedException, BadRequestException {
        authenticateService.authenticate(session);
        String uid = (String) session.getAttribute(Constants.UID);
        Course course = courseService.getCourseById(bean.getCourseid());
        if(course.getCourseCoordinator().getId().equals(uid)) {
            courseService.editCourse(bean);
        }else{
            authenticateService.authenticate(session, COURSE);
            courseService.editCourse(bean);
        }
    }
}
