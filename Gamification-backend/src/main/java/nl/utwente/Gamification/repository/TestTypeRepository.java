package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.TestType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public interface TestTypeRepository extends CrudRepository<TestType, Long> {

//    @Modifying(clearAutomatically = true)
//    @Query("UPDATE TestType t SET t = :testType WHERE t.id = :id")
//    int updateTestType(@Param("id") long id, @Param("testType") TestType testType);

}
