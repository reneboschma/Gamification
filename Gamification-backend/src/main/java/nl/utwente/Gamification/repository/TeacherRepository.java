package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.Teacher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TeacherRepository extends CrudRepository<Teacher, String> {

    @Query("select t from Teacher t where t.mainEmail = :email or :email member of t.secondaryEmails")
    Teacher findTeacherByEmail(@Param("email") String email);

    Teacher findTeacherById(String id);

    @Query("SELECT CASE WHEN COUNT(t) > 0 THEN true ELSE false END FROM Teacher t WHERE t.mainEmail = :email or :email member of t.secondaryEmails")
    boolean isEmailUsed(@Param("email") String email);

    @Query("SELECT t FROM Teacher t WHERE t.mainEmail = :email or :email member of t.secondaryEmails")
    List<Teacher> getTeachersByUsedEmail(@Param("email") String email);

    @Query("SELECT CASE WHEN COUNT(t) > 0 THEN true ELSE false END FROM Teacher t WHERE t.id <> :id and (t.mainEmail = :email or :email member of t.secondaryEmails)")
    boolean isEmailUsedByDifferentTeacher(@Param("id") String ownId, @Param("email") String email);
}
