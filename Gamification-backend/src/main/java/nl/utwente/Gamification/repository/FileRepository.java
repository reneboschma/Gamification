package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRepository extends CrudRepository<File, Long>{
}
