package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.questions.ClosedQuestion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClosedQuestionRepository extends CrudRepository<ClosedQuestion, Long> {
}
