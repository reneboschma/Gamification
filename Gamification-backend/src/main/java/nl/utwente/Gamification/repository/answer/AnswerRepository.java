package nl.utwente.Gamification.repository.answer;

import nl.utwente.Gamification.model.bean.IdObject;
import nl.utwente.Gamification.model.bean.answer.AnswerBean;
import nl.utwente.Gamification.model.db.answer.Answer;
import nl.utwente.Gamification.model.db.answer.AnswerId;
import nl.utwente.Gamification.model.db.answer.OpenAnswer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Set;

//@Repository
//public interface AnswerRepository extends CrudRepository<AnswerId, OpenAnswer> {
//
//
//    Set<Answer> findAnswersByQuestionnaireInstanceId(long questionnaireId);
//
//}

@NoRepositoryBean
public interface AnswerRepository<T extends Answer> extends CrudRepository<T, AnswerId> {

//    public Set<T> findByQuestionnaireInstanceId(long questionnaireInstanceId);

}

