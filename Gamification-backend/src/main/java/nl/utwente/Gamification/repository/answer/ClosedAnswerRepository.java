package nl.utwente.Gamification.repository.answer;

import nl.utwente.Gamification.model.db.answer.ClosedAnswer;
import nl.utwente.Gamification.projection.questionnaire.answer.ClosedAnswerProjection;

import java.util.Set;

public interface ClosedAnswerRepository extends AnswerRepository<ClosedAnswer> {


    Set<ClosedAnswerProjection> findByQuestionnaireInstanceId(long id);

}
