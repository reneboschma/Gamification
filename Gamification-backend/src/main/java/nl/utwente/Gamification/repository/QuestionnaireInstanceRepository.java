package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.QuestionnaireInstance;
import nl.utwente.Gamification.projection.questionnaire.QuestionProjection;
import nl.utwente.Gamification.projection.questionnaire.QuestionnaireIdProjection;
import nl.utwente.Gamification.projection.questionnaire.QuestionnaireInstanceProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionnaireInstanceRepository extends CrudRepository<QuestionnaireInstance, Long> {


    @Query("select q.test.testName as testName, q.test.course.courseName as courseName, q.test.date as testDate , q.id as id from QuestionnaireInstance as q where q.teacher.id = :id")
    List<QuestionnaireIdProjection> getQuestionnairesToFillIn(@Param("id") String uid);

    @Query("select qu as question, a as answer from QuestionnaireInstance as q, Answer as a, Question as qu where q.id = a.questionnaireInstanceId and q.id = :id and a.questionId = qu.id")
    List<QuestionProjection> getQuestionnaireInstance(@Param("id") long questionnaireInstanceId);

}
