package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.questions.Option;
import org.springframework.data.repository.CrudRepository;

public interface OptionRepository extends CrudRepository<Option, Long> {
}
