package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends CrudRepository<Question, Long> {
}
