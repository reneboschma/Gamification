package nl.utwente.Gamification.repository.answer;

import nl.utwente.Gamification.model.db.answer.OpenAnswer;
import nl.utwente.Gamification.projection.questionnaire.answer.ClosedAnswerProjection;
import nl.utwente.Gamification.projection.questionnaire.answer.OpenAnswerProjection;

import javax.transaction.Transactional;
import java.util.Set;

@Transactional
public interface OpenAnswerRepository extends AnswerRepository<OpenAnswer> {

    Set<OpenAnswerProjection> findByQuestionnaireInstanceId(long id);

}
