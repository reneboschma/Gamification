package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.Questionnaire;
import nl.utwente.Gamification.model.db.TestType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface QuestionnaireRepository extends CrudRepository<Questionnaire, Long> {

    @Query("select q from Questionnaire q where q.testType.id = :id order by q.dateAdded desc")
    List<Questionnaire> getQuestionnairesByTestTypeId(@Param("id") long id);

}
