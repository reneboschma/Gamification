package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.Permission;
import nl.utwente.Gamification.projection.questionnaire.PermissionProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface PermissionRepository extends CrudRepository<Permission, String> {

}
