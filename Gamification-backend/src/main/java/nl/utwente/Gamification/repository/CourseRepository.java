package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.Course;
import nl.utwente.Gamification.projection.CourseProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends CrudRepository<Course, Long> {

    @Query("select c from Course c")
    List<CourseProjection> getAllCourses();

    @Query("select c from Course c where c.courseCoordinator.id = :id")
    List<CourseProjection> getOwnCourses(@Param("id") String id);

}
