package nl.utwente.Gamification.repository.answer;

import nl.utwente.Gamification.model.db.answer.UploadAnswer;
import nl.utwente.Gamification.projection.questionnaire.answer.UploadAnswerProjection;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Set;

public interface UploadAnswerRepository extends AnswerRepository<UploadAnswer> {

    @Query("select u.questionId as questionId, f.id as fileId, f.fileName as fileName from UploadAnswer as u, File as f where u.file.id = f.id and u.questionnaireInstanceId = :id")
    Set<UploadAnswerProjection> findByQuestionnaireInstanceId(@Param("id") long id);

}
