package nl.utwente.Gamification.repository;

import nl.utwente.Gamification.model.db.Test;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRepository extends CrudRepository<Test, Long> {
}
