package nl.utwente.Gamification;

import nl.utwente.Gamification.model.db.*;
import nl.utwente.Gamification.model.db.questions.ClosedQuestion;
import nl.utwente.Gamification.model.db.questions.OpenQuestion;
import nl.utwente.Gamification.model.db.questions.Option;
import nl.utwente.Gamification.model.db.questions.UploadQuestion;
import nl.utwente.Gamification.repository.*;
import nl.utwente.Gamification.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.*;

@Component
public class ApplicationStartupListener implements
        ApplicationListener<ContextRefreshedEvent> {


    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private ClosedQuestionRepository closedQuestionRepository;

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private QuestionnaireInstanceRepository questionnaireInstanceRepository;

    @Autowired
    private TestTypeRepository testTypeRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        System.out.println("Event Happenned!!!");
//        Teacher teacher = new Teacher("test", "test@gmail.com");
//
//        repository.save(teacher);
        Teacher teacher = new Teacher("reneboschma@hotmail.com", "reneboschma@hotmail.com");
        teacherRepository.save(teacher);

        Teacher t2 = new Teacher("testuser@gmail.com", "testuser@gmail.com");
        teacherRepository.save(t2);
//        Teacher teacher = teacherRepository.findOne("reneboschma@hotmail.com");

        TestType testType = new TestType();
        testType.setShortname("Test Type");
        testType.setDescription("Type used for testing, will be removed.");
        testTypeRepository.save(testType);


        Questionnaire questionnaire = new Questionnaire();
        questionnaire.setName("Test Questionnaire");
        questionnaire.setTestType(testType);

        ClosedQuestion closedQuestion = new ClosedQuestion();
        closedQuestion.setExplanation("This is an example closed question.");
        closedQuestion.setStatement("Please select one of the option to test");


        Option optionYes = new Option();
        optionYes.setOption("Yes");

        OpenQuestion subOpenQuestion = new OpenQuestion();
        subOpenQuestion.setStatement("Please supply some information");
        optionYes.setFollowUpQuestion(subOpenQuestion);

        Option optionNo = new Option();
        optionNo.setOption("No");

        Option optionNA = new Option();
        optionNA.setOption("N/A");


        ClosedQuestion closedSub2 = new ClosedQuestion();
        closedSub2.setExplanation("This is an example sub closed question.");
        closedSub2.setStatement("Please select one of the options for this subquestion to test");

        Option sub2Yes = new Option();
        sub2Yes.setOption("Yes");

        Option sub2NA = new Option();
        sub2NA.setOption("N/A");
        closedSub2.setOptions(Arrays.asList(sub2Yes, sub2NA));

        optionNo.setFollowUpQuestion(closedSub2);

        closedQuestion.setOptions(Arrays.asList(optionYes, optionNo, optionNA));

        OpenQuestion openQuestion = new OpenQuestion();
        openQuestion.setStatement("Test open question");

        UploadQuestion uploadQuestion = new UploadQuestion();
        uploadQuestion.setStatement("Sample upload question");

        questionnaire.setQuestions(Arrays.asList(closedQuestion, openQuestion, uploadQuestion));
        questionnaireRepository.save(questionnaire);

        Course course = createCourse(teacher);
        createQuestionnaireInstance(questionnaire, teacher, createTest(course, "TestName", teacher, testType));

        createQuestionnaireInstance(questionnaire, t2, createTest(course, "Tester Course", t2, testType));

    }

    private Course createCourse(Teacher coordinator) {
        Course course = new Course();
        course.setCourseName("TestCourse");
        course.setCourseCoordinator(coordinator);
        course.setYear(2017);
        course.setCourseCode("132456");
        courseRepository.save(course);
        return course;
    }

    private Test createTest(Course course, String testName, Teacher responsible, TestType testType){
        Test test = new Test();
        test.setDate(new Date(new java.util.Date().getTime()));
        test.setCourse(course);
        test.setTestName(testName);
        test.setMainResponsible(responsible);
        test.setType(testType);
        testRepository.save(test);
        return test;
    }

    private void createQuestionnaireInstance(Questionnaire questionnaire, Teacher teacher, Test test){
        QuestionnaireInstance questionnaireInstance = new QuestionnaireInstance();
        questionnaireInstance.setQuestionnaire(questionnaire);
        questionnaireInstance.setTeacher(teacher);
        questionnaireInstance.setTest(test);
        questionnaireInstanceRepository.save(questionnaireInstance);
    }
}
