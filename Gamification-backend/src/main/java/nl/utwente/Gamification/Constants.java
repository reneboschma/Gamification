package nl.utwente.Gamification;

import java.io.File;

public class Constants {

    //Constants for session data
    public static final String UID = "uid";
    public static final String FIRSTNAME = "firstname";
    public static final String LASTNAME = "lastname";


    //Constants for file upload
    public static final String UPLOAD_PATH = "upload"+File.separator;
    public static final long UPLOAD_SIZE_LIMIT = 1000000000;



    //Permissions
    public static final String ACTIVATED = "activated";
    /**
     * Indicates the permission that one is allowed to change permissions of users
     */
    public static final String PERMISSION = "permission";

    /**
     * Indicates whether one is allowed to add or alter testtypes
     */
    public static final String TESTTYPE = "testtype";


    /**
     * Indicates whether one is allowed to add or alter tests
     */
    public static final String TEST = "test";


    /**
     * Indicates whether one is allowed to download files of other people
     */
    public static final String FILE = "file";

    /**
     * Indicates whether one is allowed to upload and/or alter questionnaires
     */
    public static final String QUESTIONNAIRE = "questionnaire";

    /**
     * Indicates whether one is allowed to add a new course
     */
    public static final String COURSE = "course";


}
