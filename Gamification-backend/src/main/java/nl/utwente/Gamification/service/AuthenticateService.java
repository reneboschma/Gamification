package nl.utwente.Gamification.service;

import nl.utwente.Gamification.Constants;
import nl.utwente.Gamification.exception.NotAuthorizedException;
import nl.utwente.Gamification.model.db.Permission;
import nl.utwente.Gamification.model.db.Teacher;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import static nl.utwente.Gamification.Constants.*;

@Service
public class AuthenticateService {


    private boolean isAuthenticated(HttpSession session, String foundId) {
        String uid = (String) session.getAttribute(Constants.UID);
        return uid != null && uid.equals(foundId);
    }

    /**
     * Function that checks if a given user is authenticated. This method extracts the user id from the session and checks
     * if this is corresponds to the foundId (the id that needs to match.
     * @param session session of the user
     * @param foundId the required id found to execute the checking method
     * @param faultMessage the string that needs to be contained inside the NotAuthorizedException
     * @throws NotAuthorizedException if the given session does not meet the foundId
     */
    public void authenticate(HttpSession session, String foundId, String faultMessage) throws NotAuthorizedException {
        if (!isAuthenticated(session, foundId)) {
            throw new NotAuthorizedException(faultMessage);
        }
    }

    /**
     * Function that checks whether all permissions are set in the session. On default it will check whether the user is
     * activated.
     * @param session HttpSession object
     * @param permisions list of String permissions needed to execute the given operation.
     * @throws NotAuthorizedException whenever one of the permissions is not set in the session data. Error message will
     * state which permission was lacking.
     */
    public void authenticate(HttpSession session, String... permisions) throws NotAuthorizedException {
        if(!(boolean)session.getAttribute(ACTIVATED)){
            throw new NotAuthorizedException("User is not allowed to make this request, permission lacking: "+ACTIVATED);
        }
        for(String s : permisions){
            if(!(boolean)session.getAttribute(s)){
                throw new NotAuthorizedException("User is not allowed to make this request, permission lacking: "+s);
            }
        }
    }

    /**
     * Saves the permissions to the session
     * @param session session to use
     * @param teacher user of which the session data needs to be set
     */
    public void setPermissionDataSession(HttpSession session, Teacher teacher){
        Permission p = teacher.getPermission();
        session.setAttribute(ACTIVATED, p.isActivated());
        session.setAttribute(FILE, p.isFile());
        session.setAttribute(PERMISSION, p.isPermission());
        session.setAttribute(TEST, p.isTest());
        session.setAttribute(TESTTYPE, p.isTesttype());
        session.setAttribute(QUESTIONNAIRE, p.isQuestionnaire());
        session.setAttribute(COURSE, p.isCourse());
    }

}
