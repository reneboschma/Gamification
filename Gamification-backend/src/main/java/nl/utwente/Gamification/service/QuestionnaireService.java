package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.QuestionnaireBean;
import nl.utwente.Gamification.model.db.Questionnaire;
import nl.utwente.Gamification.model.db.TestType;
import nl.utwente.Gamification.repository.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionnaireService {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;

    @Autowired
    private TestTypeService testTypeService;

    public void addNewQuestionnaire(QuestionnaireBean questionnaire) throws NotFoundException {
        TestType testType = testTypeService.getTestType(questionnaire.getTestTypeId());

        Questionnaire q = new Questionnaire();
        q.setQuestions(questionnaire.getQuestions());
        q.setTestType(testType);
        q.setName(questionnaire.getName());

        questionnaireRepository.save(q);
    }

    /**
     * Returns the specified questionnaire belonging to the specified typeId. The questionnaire that is added latest will
     * be returned.
     * @param typeId
     * @return the newest questionnaire associated to this type
     * @throws NotFoundException if no questionnaire could be found
     */
    public Questionnaire getQuestionnaireByType(long typeId) throws NotFoundException {
        List<Questionnaire> questionnaires = questionnaireRepository.getQuestionnairesByTestTypeId(typeId);
        if(questionnaires == null || questionnaires.size() == 0){
            throw new NotFoundException("Could not find specified Questionnaire");
        }
        return questionnaires.get(0);
    }

}
