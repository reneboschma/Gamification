package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.TeacherBean;
import nl.utwente.Gamification.model.db.Teacher;
import nl.utwente.Gamification.repository.TeacherRepository;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

import static nl.utwente.Gamification.Constants.UID;

@Service
public class TeacherService {


    @Autowired
    private TeacherRepository teacherRepository;

    /**
     * Adds a new teacher to the system. First check whether the id is already present in the system
     * @param id the id retrieved from the identity provider
     * @param email the main email of this user
     */
    public Teacher addTeacher(String id, String email){
        Teacher teacher;
        try {
            teacher = getTeacherById(id);
        } catch (NotFoundException e) {
            teacher = new Teacher(id, email);
            teacherRepository.save(teacher);
        }
        return teacher;
    }

    /**
     * Retrieves the Teacher by email. This can be either the primary email or belonging to the secondary emails
     * @param email email of the teacher
     * @return the specified teacher
     * @throws NotFoundException if the teacher could not be found.
     */
    public Teacher getTeacherByEmail(String email) throws NotFoundException {
        Teacher teacher = teacherRepository.findTeacherByEmail(email);
        if(teacher ==  null){
            throw new NotFoundException("Could not find the teacher belonging to specified email");
        }
        return teacher;
    }

    /**
     * Retrieves the Teacher by id.
     * @param id of the teacher
     * @return the specified teacher
     * @throws NotFoundException if the teacher could not be found
     */
    public Teacher getTeacherById(String id) throws NotFoundException {
        Teacher teacher = teacherRepository.findTeacherById(id);
        if(teacher ==  null){
            throw new NotFoundException("Could not find the teacher belonging to specified id");
        }
        return teacher;
    }

    /**
     * Retrieves the information about the specified user
     * @param id of the teacher
     * @return Information about the specified teacher such as mainEmail, secondaryEmail, id and permissions
     * @throws NotFoundException if the specified teacher could not be found
     */
    public TeacherBean getTeacherInformation(String id) throws NotFoundException {
        Teacher teacher = getTeacherById(id);
        TeacherBean b = new TeacherBean();
        b.setMainEmail(teacher.getMainEmail());
        b.setId(id);
        b.setSecondaryEmails(teacher.getSecondaryEmails());
        b.setPermissions(teacher.getPermission());
        return b;
    }

    /**
     * Adds a secondary email address to the specified teacher.
     * @param id of the teacher
     * @param email to add
     * @throws BadRequestException if the email is not a valid email or when the address is already in use (by the teacher or a different teacher)
     * @throws NotFoundException if the specified teacher could not be found
     */
    public void addSecondaryEmail(String id, String email) throws BadRequestException, NotFoundException {
        if(EmailValidator.getInstance().isValid(email)){
            Teacher teacher = getTeacherById(id);
            if(!teacherRepository.isEmailUsed(email)){
                teacher.getSecondaryEmails().add(email);
                teacherRepository.save(teacher);
            }else{
                throw new BadRequestException("Specified email is already in use");
            }
        }else{
            throw new BadRequestException("Specified email is not a valid email address");
        }
    }

    /**
     * Removes the specified email from the secondary emails of a teacher
     * @param id of the teacher
     * @param email to be removed
     * @throws NotFoundException if the teacher could not be found
     * @throws BadRequestException if the email was not present as secondary email
     */
    public void removeSecondaryEmail(String id, String email) throws NotFoundException, BadRequestException {
        Teacher teacher = getTeacherById(id);
        if(teacher.getSecondaryEmails().contains(email)){
            teacher.getSecondaryEmails().remove(email);
            teacherRepository.save(teacher);
        }else{
            throw new BadRequestException("Specified email is not present as secondary email");
        }
    }

    /**
     * Sets the main email of the specified teacher. If the email is present as secondary email, it will be removed from
     * there. Also a check is done to see if the specified email is not already in use by a different teacher
     * @param id of the teacher
     * @param email to be set
     * @throws NotFoundException
     * @throws BadRequestException
     */
    public void setPrimaryEmail(String id, String email) throws NotFoundException, BadRequestException {
        if(EmailValidator.getInstance().isValid(email)){
            Teacher teacher = getTeacherById(id);
            if(teacherRepository.isEmailUsedByDifferentTeacher(id, email)){
                throw new BadRequestException("Specified email is already in use");
            }
            if(teacher.getSecondaryEmails().contains(email)){
                teacher.getSecondaryEmails().remove(email);
            }
            teacher.setMainEmail(email);
            teacherRepository.save(teacher);
        }else{
            throw new BadRequestException("Specified email is not a valid email address");
        }
    }


}
