package nl.utwente.Gamification.service;

import nl.utwente.Gamification.model.db.Questionnaire;
import nl.utwente.Gamification.model.db.QuestionnaireInstance;
import nl.utwente.Gamification.model.db.Teacher;
import nl.utwente.Gamification.model.db.Test;
import nl.utwente.Gamification.repository.QuestionnaireInstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionnaireInstanceService {

    @Autowired
    private QuestionnaireInstanceRepository questionnaireInstanceRepository;

    /**
     * Adds a new QuestionnaireInstance to the database. This is done using the supplied arguments.
     * @param teacher to add a questionnaire instance for
     * @param questionnaire the questoinnaire that needs to be filled in
     * @param test the test to which this questionnaire belongs
     */
    public void addQuestionnaireInstacne(Teacher teacher, Questionnaire questionnaire, Test test){
        QuestionnaireInstance q = new QuestionnaireInstance();
        q.setTest(test);
        q.setTeacher(teacher);
        q.setQuestionnaire(questionnaire);
        questionnaireInstanceRepository.save(q);
    }

}
