package nl.utwente.Gamification.service;

import com.google.common.collect.Lists;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.TestTypeBean;
import nl.utwente.Gamification.model.db.TestType;
import nl.utwente.Gamification.repository.TestTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestTypeService {

    @Autowired
    private TestTypeRepository testTypeRepository;

    /**
     * Saves the given testtype to the database
     * @param testTypeBean the TestTypeBean
     */
    public void addTestType(TestTypeBean testTypeBean){
        TestType testType = new TestType();
        testType.setShortname(testTypeBean.getShortname());
        testType.setDescription(testTypeBean.getDescription());
        testTypeRepository.save(testType);
    }

    /**
     * Returns the requested TestType
     * @param id of the requested TestType
     * @return the TestTYpe
     * @throws NotFoundException if the id could not be found
     */
    public TestType getTestType(long id) throws NotFoundException {
        TestType testType = testTypeRepository.findOne(id);
        if (testType == null) {
            throw new NotFoundException("Could not find requested TestType.");
        }
        return testType;
    }

    /**
     * Returns all testtypes located in the database
     * @return list of testtypes
     */
    public List<TestType> getTestTypes(){
        return Lists.newArrayList(testTypeRepository.findAll());
    }

    /**
     * Updates the specified testtype to the given testtype.
     * @param testType to alter to, the id specified is used to replace the testttype
     * @throws NotFoundException if the specified id is not a known testtype
     */
    public void updateTestType(TestType testType) throws NotFoundException {
        TestType t = getTestType(testType.getId());
        t.setDescription(testType.getDescription());
        t.setShortname(testType.getShortname());
        testTypeRepository.save(t);
    }

}
