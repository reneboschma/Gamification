package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.TestBean;
import nl.utwente.Gamification.model.db.*;
import nl.utwente.Gamification.repository.TeacherRepository;
import nl.utwente.Gamification.repository.TestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TestService {

    @Autowired
    private TestRepository testRepository;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private TestTypeService testTypeService;

    @Autowired
    private QuestionnaireInstanceService questionnaireInstanceService;

    @Autowired
    private QuestionnaireService questionnaireService;

    @Autowired
    private CourseService courseService;

    /**
     * Adds a new test to the database. The test is only added when all users defined in the testBean can be resolved.
     * Also a valid testtype Id must be supplied and at least one person needs to be specified to fill in a questionnaire.
     * Furthermore, the main responsible must have logged in into the system.
     * @param testBean
     * @throws BadRequestException
     * @throws NotFoundException
     */
    public void addTest(TestBean testBean) throws BadRequestException, NotFoundException {
        if(testBean.getTeachersToFillInIds().size() == 0){
            throw new BadRequestException("At least one user needs to be specified to fill in a questionnaire");
        }

        // check if mainRes has logged into the system, if not a not found will be thrown by this function call.
//        Teacher mainRes = teacherService.getTeacherById(testBean.getMainResponsibleId());

        Teacher mainResponsible = teacherService.getTeacherById(testBean.getMainResponsibleId());
        TestType testType = testTypeService.getTestType(testBean.getTestTypeId());
        Course course = courseService.getCourseById(testBean.getCourseId());

        List<Teacher> teachers = new ArrayList<>();
        for (String s : testBean.getTeachersToFillInIds()){
            try {
                teachers.add(teacherService.getTeacherById(s));
            }catch (NotFoundException e){
                Teacher teacher = teacherService.addTeacher(s, null);
                teachers.add(teacher);
            }
        }

        Questionnaire questionnaire = questionnaireService.getQuestionnaireByType(testType.getId());

        Test test = new Test();
        test.setTestName(testBean.getTestName());
        test.setType(testType);
        test.setMainResponsible(mainResponsible);
        test.setDate(testBean.getDate());
        test.setCourse(course);

        testRepository.save(test);


        for(Teacher t : teachers){
            questionnaireInstanceService.addQuestionnaireInstacne(t, questionnaire, test);
        }
    }

}
