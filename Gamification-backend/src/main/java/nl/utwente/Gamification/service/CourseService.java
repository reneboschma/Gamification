package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.CourseBean;
import nl.utwente.Gamification.model.bean.CourseEditBean;
import nl.utwente.Gamification.model.db.Course;
import nl.utwente.Gamification.model.db.Teacher;
import nl.utwente.Gamification.projection.CourseProjection;
import nl.utwente.Gamification.repository.CourseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private TeacherService teacherService;

    /**
     * Adds the new course to the database.
     * @param courseBean
     * @throws NotFoundException if the specified teacher could not be found
     * @throws BadRequestException if the request was malformed (i.e. negative year was supplied)
     */
    public void addCourse(CourseBean courseBean) throws NotFoundException, BadRequestException {
        Teacher coordinator = teacherService.getTeacherById(courseBean.getCourseCoordinatorId());
        addCourse(courseBean.getCourseName(), courseBean.getCourseCode(), courseBean.getYear(), coordinator);
    }


    private void addCourse(String courseName, String courseCode, int year, Teacher courseCoordinator) throws BadRequestException {
        if(year < 0){
            throw new BadRequestException("Year below zero not possible");
        }
        Course course = new Course();
        course.setCourseName(courseName);
        course.setCourseCode(courseCode);
        course.setYear(year);
        course.setCourseCoordinator(courseCoordinator);

        courseRepository.save(course);
    }

    /**
     * Edits the given course. All values specified are overwritten. It is not possible to change the main responsible
     * teacher of a course.
     * @param bean containing the information to alter
     * @throws NotFoundException if the course could not be found
     */
    public void editCourse(CourseEditBean bean) throws NotFoundException, BadRequestException {
        if(bean.getYear() < 0){
            throw new BadRequestException("Year below zero not possible");
        }
        Course course = getCourseById(bean.getCourseid());
        course.setCourseCode(bean.getCourseCode());
        course.setCourseName(bean.getCourseName());
        course.setYear(bean.getYear());

        courseRepository.save(course);
    }

    /**
     * Retrieves a given course
     * @param id of the course. This is the internal id, not the courseCode!
     * @return the course if present
     * @throws NotFoundException if the course could not be found
     */
    public Course getCourseById(long id) throws NotFoundException {
        Course course = courseRepository.findOne(id);
        if(course == null){
            throw new NotFoundException("Could not find specified course");
        }
        return course;
    }

    public List<CourseProjection> getAllCourses(){
        return courseRepository.getAllCourses();
    }

    public List<CourseProjection> getAllOwnCourses(String id){
        return courseRepository.getOwnCourses(id);
    }

}
