package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.BadRequestException;
import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.bean.answer.AnswerBean;
import nl.utwente.Gamification.model.bean.answer.ClosedAnswerBean;
import nl.utwente.Gamification.model.bean.answer.OpenAnswerBean;
import nl.utwente.Gamification.model.bean.answer.UploadAnswerBean;
import nl.utwente.Gamification.model.db.Question;
import nl.utwente.Gamification.model.db.answer.*;
import nl.utwente.Gamification.model.db.questions.ClosedQuestion;
import nl.utwente.Gamification.model.db.questions.Option;
import nl.utwente.Gamification.projection.questionnaire.QuestionProjection;
import nl.utwente.Gamification.projection.questionnaire.answer.AnswerProjection;
import nl.utwente.Gamification.repository.*;
import nl.utwente.Gamification.repository.answer.ClosedAnswerRepository;
import nl.utwente.Gamification.repository.answer.OpenAnswerRepository;
import nl.utwente.Gamification.repository.answer.UploadAnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AnswerService {


    @Autowired
    private OpenAnswerRepository openAnswerRepository;

    @Autowired
    private FileService fileService;

    @Autowired
    private UploadAnswerRepository uploadAnswerRepository;

    @Autowired
    private OptionService optionService;

    @Autowired
    private ClosedAnswerRepository closedAnswerRepository;

    @Autowired
    private QuestionnaireInstanceRepository questionnaireInstanceRepository;

    @Autowired
    private QuestionService questionService;

    /**
     * Adds the given answer to the database. In case the answer is for a closed question, it first removes all subanswers
     * to keep the data in sync with what is actual answered. Then it overwrites (or inserts) the current answer).
     * @param answers List of answers. This list must be in order of depth of the question. This means that closed parent
     *                closed question for instance must be the first element of the list, otherwise the just given answer
     *                of the child question will be deleted.
     * @throws NotFoundException if the given fileId or optionId in the uploadAnswer or optionAnswer can not be found in the database.
     * @throws BadRequestException if one of the supplied answers does not match the type of the corresponding question
     */
    public void addAnswer(List<AnswerBean> answers) throws NotFoundException, BadRequestException {
        for(AnswerBean answer : answers) {
            //Check if question type matches
            Question question = questionService.getQuestion(answer.getQuestionId());
            if(!question.getType().matchesAnswerBean(answer)){
                throw new BadRequestException("Given type of answer does not match the type of question");
            }
        }

        for(AnswerBean answer : answers) {
            if (answer instanceof ClosedAnswerBean) {
                ClosedAnswerBean closedBean = (ClosedAnswerBean) answer;
                ClosedAnswer closedAnswer = new ClosedAnswer();

                closedAnswer.setQuestionId(answer.getQuestionId());
                closedAnswer.setQuestionnaireInstanceId(answer.getQuestionnaireInstanceId());
                Option option = optionService.getOptionById(closedBean.getOptionId());
                closedAnswer.setOption(option);


                Set<Long> answersToRemove = getQuestionsIdsOfQuestion(questionService.getQuestion(answer.getQuestionId()));
                answersToRemove.add(answer.getQuestionId());

                //Remove all answers including its followUpQuestions
                //TODO test nested removal of different types of followUpQuestions
                AnswerId answerId = new AnswerId();
                answerId.setQuestionnaireInstanceId(answer.getQuestionnaireInstanceId());
                for (long questionId : answersToRemove) {
                    answerId.setQuestionId(questionId);
                    if (closedAnswerRepository.exists(answerId)) {
                        closedAnswerRepository.delete(answerId);
                    }else if (openAnswerRepository.exists(answerId)) {
                        openAnswerRepository.delete(answerId);
                    }else if (uploadAnswerRepository.exists(answerId)) {
                        uploadAnswerRepository.delete(answerId);
                    }
                }

                closedAnswerRepository.save(closedAnswer);

            } else if (answer instanceof UploadAnswerBean) {
                UploadAnswerBean uploadBean = (UploadAnswerBean) answer;
                UploadAnswer uploadAnswer = new UploadAnswer();

                uploadAnswer.setQuestionId(uploadBean.getQuestionId());
                uploadAnswer.setQuestionnaireInstanceId(uploadBean.getQuestionnaireInstanceId());
                uploadAnswer.setFile(fileService.getFileById(uploadBean.getFileId()));

                AnswerId answerId = new AnswerId(answer.getQuestionnaireInstanceId(), answer.getQuestionId());
                if (uploadAnswerRepository.exists(answerId)) {
                    uploadAnswerRepository.delete(answerId);
                }
                uploadAnswerRepository.save(uploadAnswer);
            } else if (answer instanceof OpenAnswerBean) {
                OpenAnswerBean openBean = (OpenAnswerBean) answer;
                OpenAnswer openAnswer = new OpenAnswer();

                openAnswer.setContent(openBean.getContent());
                openAnswer.setQuestionId(openBean.getQuestionId());
                openAnswer.setQuestionnaireInstanceId(openBean.getQuestionnaireInstanceId());

                //remove previous answer if exists
                AnswerId answerId = new AnswerId(openAnswer.getQuestionnaireInstanceId(), answer.getQuestionId());
                if (openAnswerRepository.exists(answerId)) {
                    openAnswerRepository.delete(answerId);
                }
                openAnswerRepository.save(openAnswer);
            } else {
                throw new RuntimeException("Unknown answerBean type supplied");
            }
        }
    }

    private Set<Long> getQuestionsIdsOfQuestion(Question question) {
        Set<Long> result = new HashSet<>();
        if (question != null){
            result.add(question.getId());
            if(question instanceof ClosedQuestion){
                for(Option option : ((ClosedQuestion) question).getOptions()) {
                    result.addAll(getQuestionsIdsOfQuestion(option.getFollowUpQuestion()));
                }
            }
        }
        return result;
    }

    public Set<AnswerProjection> getAnswers(long questionnaireInstanceId) {
        Set<AnswerProjection> result = new HashSet<>();
//        result.addAll(uploadAnswerRepository.findByQuestionnaireInstanceId(questionnaireInstanceId));
//        result.addAll(closedAnswerRepository.findByQuestionnaireInstanceId(questionnaireInstanceId));
//        result.addAll(openAnswerRepository.findByQuestionnaireInstanceId(questionnaireInstanceId));
        result.addAll(uploadAnswerRepository.findByQuestionnaireInstanceId(questionnaireInstanceId));
        result.addAll(closedAnswerRepository.findByQuestionnaireInstanceId(questionnaireInstanceId));
        result.addAll(openAnswerRepository.findByQuestionnaireInstanceId(questionnaireInstanceId));

//        return answerRepository.findAnswersByQuestionnaireInstanceId(questionnaireInstanceId);
        return result;
    }

    @Deprecated
    public List<QuestionProjection> getQuestionnaireInstance(long questionnaireInstanceId){
        return questionnaireInstanceRepository.getQuestionnaireInstance(questionnaireInstanceId);
    }
}
