package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.db.questions.Option;
import nl.utwente.Gamification.repository.OptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OptionService {

    @Autowired
    private OptionRepository optionRepository;

    public Option getOptionById(long optionId) throws NotFoundException {
        Option option = optionRepository.findOne(optionId);
        if (option == null){
            throw new NotFoundException("Could not find the given option.");
        }
        return option;
    }

}
