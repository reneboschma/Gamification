package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.db.File;
import nl.utwente.Gamification.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileService {

    @Autowired
    private FileRepository fileRepository;

    /**
     * Retrieves the given file belonging to the supplied id.
     * @param fileId id of the file
     * @return File object from the database
     * @throws NotFoundException if the given fileId can not be found in the database
     */
    public File getFileById(long fileId) throws NotFoundException {
        File file = fileRepository.findOne(fileId);
        if (file == null){
            throw new NotFoundException("Given file could not be found.");
        }
        return file;
    }

}
