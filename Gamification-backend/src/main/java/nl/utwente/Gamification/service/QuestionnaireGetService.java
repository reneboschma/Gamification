package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.db.Questionnaire;
import nl.utwente.Gamification.model.db.QuestionnaireInstance;
import nl.utwente.Gamification.projection.questionnaire.QuestionnaireIdProjection;
import nl.utwente.Gamification.repository.QuestionnaireInstanceRepository;
import nl.utwente.Gamification.repository.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class QuestionnaireGetService {

    @Autowired
    private QuestionnaireInstanceRepository questionnaireInstanceRepository;

    public List<QuestionnaireIdProjection> getQuestionnairesToFillIn(String uid){
        return questionnaireInstanceRepository.getQuestionnairesToFillIn(uid);
    }


    public QuestionnaireInstance getQuestionnaireInstance(long questionnaireInstanceId) throws NotFoundException {
        QuestionnaireInstance instance = questionnaireInstanceRepository.findOne(questionnaireInstanceId);
        if(instance == null){
            throw new NotFoundException("Given QuestionnaireInstance not found");
        }
        return instance;
    }
}
