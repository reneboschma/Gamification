package nl.utwente.Gamification.service;

import nl.utwente.Gamification.exception.NotFoundException;
import nl.utwente.Gamification.model.db.Question;
import nl.utwente.Gamification.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    public Question getQuestion(long questionId) throws NotFoundException {
        Question question = questionRepository.findOne(questionId);
        if(question == null){
            throw new NotFoundException("Could not find specified question");
        }
        return question;
    }

}
