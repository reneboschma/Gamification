package nl.utwente.Gamification.configuration;

import nl.utwente.Gamification.utils.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.resource.ResourceTransformer;
import org.springframework.web.servlet.resource.ResourceTransformerChain;
import org.springframework.web.servlet.resource.TransformedResource;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Component
public class JsTransformer implements ResourceTransformer {

    @Value("${server.url}")
    private String url;

    @Override
    public Resource transform(HttpServletRequest request, Resource resource, ResourceTransformerChain transformerChain) throws IOException {
        String js = IOUtils.readResource(resource);
        js = js.replace("$URL", url);
        Resource newResource = new TransformedResource(resource, js.getBytes(StandardCharsets.UTF_8));
        return transformerChain.transform(request, newResource);
    }
}
