package nl.utwente.Gamification.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.resource.PathResourceResolver;

@Configuration
@Profile({"dev", "dev-jit"})
public class DevConfig extends WebMvcConfigurerAdapter {

    private final JsTransformer jsTransformer;

    @Autowired
    public DevConfig(JsTransformer jsTransformer) {
        this.jsTransformer = jsTransformer;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/**/*.js")
                .addResourceLocations("/", "classpath:/BOOT-INF/classes/static/")
                .resourceChain(true)
                .addResolver(new PathResourceResolver())
                .addTransformer(jsTransformer);
    }
}