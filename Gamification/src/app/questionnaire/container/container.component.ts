import {Component, Input, OnChanges, OnInit, QueryList, SimpleChanges, ViewChildren} from '@angular/core';
import {QUESTION_CLOSED, QUESTION_OPEN, QUESTION_UPLOAD} from "../../constants";
import {EntryComponent} from "../question-entries/entry/entry.component";
import {HttpClient} from "@angular/common/http";
import {QuestionnaireRetrieverService} from "../../services/questionnaire/questionnaire-retriever.service";
import {Answer, AnswerServiceService} from "../../services/questionnaire/answer/answer-service.service";
import {ErrorMessageService} from "../../services/util/error/error-message.service";
import {ToastService} from "../../services/util/toast/toast.service";
import {ParentEntryComponent} from "../question-entries/parent-entry/parent-entry.component";
import {Router} from "@angular/router";

@Component({
  selector: 'app-questionnaire-container',
  templateUrl: './container.component.html',
  styleUrls: ['./container.component.css'],
  providers: [QuestionnaireRetrieverService, AnswerServiceService, ErrorMessageService]
})
export class ContainerComponent implements OnInit, OnChanges {

  @ViewChildren(ParentEntryComponent) customComponentChildren: QueryList<ParentEntryComponent>;


  ngOnChanges(changes: SimpleChanges): void {
    // console.log("Container changed");
  }


  @Input()
  questionnaireInstanceId;

  childFinishedArray: boolean[] = [];

  constructor(private questionnaireRetriever: QuestionnaireRetrieverService,
              private answerService: AnswerServiceService,
              private errorMessage: ErrorMessageService,
              private toastService: ToastService,
              private router: Router) {

  }

  ngOnInit() {
    this.answerMap = new Map<number, Answer>();
    this.answerService.getAnswers(this.questionnaireInstanceId).subscribe(
      answerData => {
        this.answers = answerData;
        console.log("result of getAnswers: "+JSON.stringify(answerData));
        for (let answer of this.answers){
          this.answerMap[answer.questionId] = answer;
          console.log("answerMap: "+JSON.stringify(this.answerMap));
        }

        this.questionnaireRetriever.getQuestionnaireFromQuestionnaireInstance(this.questionnaireInstanceId).subscribe(
            data => {
              this.questions = data.questions;
              this.questions.forEach(() => this.childFinishedArray.push(false))
            }
          )
      },
      error => this.toastService.setMessage(error.error.message, 'danger')
    );

  }

  questions;

  answers: Answer[];
  answerMap: Map<number, Answer>;


  getAnswers(): Map<number, Answer> {
    return this.answerMap;
  }

  childFinished(index: number){
    this.childFinishedArray[index] = true;
  }

  isFinished(): boolean {
    let ret = true;
    this.childFinishedArray.forEach((value) => {
      if(!value){
        ret = false;
      }
    });
    ret = ret && this.childFinishedArray.length > 0;
    return ret;
  }

  submit(){
    this.toastService.setMessage("Sucessfully completed questionnaire", 'success');
    this.router.navigate(['thankyou'])
  }

  questions2 = [
    {
      id: '1',
      type: 'closed',
      statement: 'Are there explicit learning outcomes for the course?',
      explanation: 'Learning goals help establish transparency in your course’s content. They establish clear expectations for the learner and from the learning material. They combine the previous knowledge with the current to create a new outcome. Learning goals set up the requirements for the new outcome and how it will hold meaning and value to the learner.',
      options: [
        {
          option: 'Yes',
          desired: true,
          followUpQuestion: {
            id: '1.1',
            type: 'closed',
            statement: 'Where can they be found?',
            options: [
              {
                option: 'On Osiris',
                desired: true
              },
              {
                option: 'Upload',
                desired: true,
                followUpQuestion: {
                  id: '1.1.2',
                  type: 'upload',
                  statement: 'Please upload the learning goals'
                }
              },
              {
                option: 'Fill In',
                desired: true,
                followUpQuestion: {
                  id: '1.1.3',
                  type: 'open',
                  statement: 'Please fill in the learning goals'
                }
              }
            ]
          }
        },
        {
          option: 'No',
          desired: false
        }
      ]
    },
    {
      id: '2',
      type: 'closed',
      statement: 'Can the test be linked to the learning outcomes?',
      explanation: 'Ideally, every test should directly establish that one or more learning outcomes have been achieved. Here you are asked to make that link explicit.',
      options: [
        {
          option: 'Yes',
          desired: true
        },
        {
          option: 'No',
          desired: false
        }
      ]
    },
    {
      id: '3',
      type: 'closed',
      statement: 'Is there a test matrix for this question?',
      explanation: 'A test matrix gives a connection of individual test questions to learning outcomes. This helps to ensure that no learning outcomes for the test are forgotten. More information here',
      options: [
        {
          option: 'Yes',
          desired: true,
          followUpQuestion: {
            id: '3.1',
            type: 'upload',
            statement: 'Please upload the test matrix.'
          }
        },
        {
          option: 'No',
          desired: false
        }
      ]
    },
    {
      id: '4',
      type: 'closed',
      statement: 'Is there an answer model for this test?',
      explanation: 'An answering model enables the test to be marked by persons other than yourself and also offers consistency in answers. Furthermore, it can be checked whether the questions are graded on sufficient deepening.',
      options: [
        {
          option: 'Yes',
          desired: true,
          followUpQuestion: {
            id: '4.1',
            type: 'upload',
            statement: 'Please upload the answer model.'
          }
        },
        {
          option: 'No',
          desired: false
        }
      ]
    },
    {
      id: '5',
      type: 'closed',
      statement: 'Where there any incident(s) during the test?',
      explanation: 'An incident may harm the results of the students.',
      options: [
        {
          option: 'Yes',
          desired: true,
          followUpQuestion: {
            id: '5.1',
            type: 'open',
            statement: 'Please describe the incident(s).'
          }
        },
        {
          option: 'No',
          desired: true
        }
      ]
    },
    {
      id: '6',
      type: 'closed',
      statement: 'Is there an analysis of the test outcome?',
      explanation: 'The analysis can consist of any comments based on the actual grades: was the test well or badly made? Were there questions that scored especially well? How do you explain these effects? More information here',
      options: [
        {
          option: 'Yes',
          desired: true,
          followUpQuestion: {
            id: '6.1',
            type: 'closed',
            statement: 'Where can it be found?',
            options: [
              {
                option: 'Upload',
                desired: true,
                followUpQuestion: {
                  id: '6.1.1',
                  type: 'upload',
                  statement: ''
                }
              },
              {
                option: 'Fill In',
                desired: true,
                followUpQuestion: {
                  id: '6.1.2',
                  type: 'open',
                  statement: 'Please fill in your analysis.'
                }
              }
            ]
          }
        },
        {
          option: 'No',
          desired: false
        }
      ]
    }
  ];



}
