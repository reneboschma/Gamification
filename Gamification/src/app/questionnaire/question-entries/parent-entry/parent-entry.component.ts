import {Component, EventEmitter, Input, Output, OnInit} from '@angular/core';
import {Answer, AnswerServiceService} from "../../../services/questionnaire/answer/answer-service.service";
import {ContainerComponent} from "../../container/container.component";
import {ErrorMessageService} from "../../../services/util/error/error-message.service";



@Component({
  selector: 'app-questionnaire-entry-parent',
  templateUrl: './parent-entry.component.html',
  styleUrls: ['./parent-entry.component.css'],
  providers: [AnswerServiceService, ErrorMessageService]
})
export class ParentEntryComponent implements OnInit {

  /**
   * Question variable passed down to the entry component
   */
  @Input()
  question;

  @Input()
  rowNumber;

  @Input()
  container: ContainerComponent;

  // @Output()
  // questionFinishedEmitter = new EventEmitter<boolean>();

  isFinished = false;

  constructor(private answerService: AnswerServiceService, private errorMessage: ErrorMessageService) { }

  ngOnInit() {
    if(this.container.getAnswers()[this.question.id]){
      this.container.childFinished(this.rowNumber);
    }
  }

  /**
   * Function executed when the question is answered with all asked sub-questions. From this the answers are send to the
   * server. The result contains two fields: isFinished and data. The first indicates wheter the question is finished or not,
   * the second contains the data that needs to be send to the server to store the actual answer.
   */
  onChange(result){
    this.isFinished = result.isFinished;
    console.log("OnChange on parent-entry, is finished: "+JSON.stringify(result));
    if(this.isFinished) {
      this.answerService.postAnswer(result.data).subscribe(
        next => {},
        error => this.errorMessage.openErrorDefault(error.error.message)
      );
      // this.questionFinishedEmitter.emit(this.isFinished);
      this.container.childFinished(this.rowNumber);
    }
  }

}
