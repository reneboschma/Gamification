import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParentEntryComponent } from './parent-entry.component';

describe('ParentEntryComponent', () => {
  let component: ParentEntryComponent;
  let fixture: ComponentFixture<ParentEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParentEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParentEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
