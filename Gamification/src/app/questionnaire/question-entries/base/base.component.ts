import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ContainerComponent} from "../../container/container.component";
import {Answer} from "../../../services/questionnaire/answer/answer-service.service";


export interface Interceptor {
  intercept(result);
}
/**
 * This class represents the base questionaire type and fullfills various functionality available to the other questionnaire types.
 */
@Component({
  selector: 'app-question-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {

  /**
   * JSON object that contains the data of the current questions displayed.
   */
  @Input()
  question;

  /**
   * Indicates the row number of the question. This does not include follow-up-questions.
   */
  @Input()
  rowNumber;

  @Input()
  container: ContainerComponent;

  /**
   * Indicates whether this entry is the first question or not.
   */
  @Input()
  isFirstQuestion = false;

  @Output()
  finishEmitter = new EventEmitter();

  @Input()
  isFinished = false;

  interceptor: Interceptor = null;

  getAnswer(): Answer{
    return this.container.getAnswers()[this.question.id];
  }

  constructor(private cdRef:ChangeDetectorRef) { }

  ngOnInit() {
  }

  /**
   * Function that gets executed whenever an answer is given. This is also emitted to the parent question entries as it
   * may be the case that a question was done but by changing the answer it is not anymore.
   * @param result the result of the calling
   */
  onChange(result){
    console.log('onChange on base, is finished: '+ JSON.stringify(result));
    this.isFinished = result.isFinished;
    this.cdRef.detectChanges();
    if(this.interceptor != null){
      console.log("interception");
      this.interceptor.intercept(result);
    }
    this.finishEmitter.emit(result);
  }

  setClasses() {
    return {
      evenRow: this.rowNumber % 2 == 0,
      oddRow: this.rowNumber % 2 != 0,
      doneRow: this.isFinished
    };
  }

}
