import {ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {BaseComponent} from "../base/base.component";

@Component({
  selector: 'app-questionnaire-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css']
})
export class EntryComponent extends BaseComponent implements OnInit {

  @Input()
  question;

  @Input()
  rowNumber;

  @Output()
  questionFinishedEmitter = new EventEmitter();

  /**
   * Indicates whether this entry is the first question or not.
   */
  @Input()
  isFirstQuestion = false;

  @Input()
  isFinished = false;

  constructor(cdRef:ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {
  }

  onChange(result){
    // this.done = true;
    console.log('onChange on entry, is finished: '+JSON.stringify(result));
    this.isFinished = result.isFinished;
    this.questionFinishedEmitter.emit(result);
  }
}
