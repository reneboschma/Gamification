import {AfterContentChecked, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BaseComponent} from "../base/base.component";
import {ClosedAnswer, OpenAnswer} from "../../../services/questionnaire/answer/answer-service.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-questionnaire-open',
  templateUrl: './open.component.html',
  styleUrls: ['./open.component.css']
})
export class OpenComponent extends BaseComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              cdRef:ChangeDetectorRef) {
    super(cdRef);
  }

  ngOnInit() {
    if(this.getAnswer()) {
      console.log("on init open, value of get answer: " + JSON.stringify(this.getAnswer()));
      this.contents = (this.getAnswer() as OpenAnswer).content;
      this.isFinished = true;
    }
  }


  contents: string;

  // getContents(){
  //   this.contents = (this.getAnswer() as OpenAnswer).content;
  //   console.log("conents equals: "+this.contents);
  //   return this.contents;
  // }

  onSubmitPress() {
    let result = {} as OpenAnswer;
    console.log("value of contents: "+this.contents);
    this.route.params.subscribe(
      params => result =
        {
          questionnaireInstanceId: params.id,
          questionId: this.question.id,
          type: "OPEN",
          content: this.contents
        }
    );

    this.onChange({isFinished: true, data: [result]});
    // this.onChange({isFinished: true, data: false});
  }
}
