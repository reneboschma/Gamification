import {ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {BaseComponent} from "../base/base.component";
import { UploadEvent, UploadFile } from 'ngx-file-drop';
import {ClosedAnswer, UploadAnswer} from "../../../services/questionnaire/answer/answer-service.service";
import {ActivatedRoute} from "@angular/router";
import {UploadService} from "../../../services/questionnaire/upload/upload.service";
import {ErrorMessageService} from "../../../services/util/error/error-message.service";
import {ToastService} from "../../../services/util/toast/toast.service";

@Component({
  selector: 'app-questionnaire-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css'],
  providers: [UploadService, ErrorMessageService]
})
export class UploadComponent extends BaseComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private upload: UploadService,
              cdRef:ChangeDetectorRef,
              private errorMessage: ErrorMessageService,
              private toastService: ToastService) {
    super(cdRef);
  }

  ngOnInit() {
    if(this.getAnswer()){
      console.log("on init open, value of get answer: " + JSON.stringify(this.getAnswer()));
      let uploadAnswer = this.getAnswer() as UploadAnswer;
      // this.uploadedFileName = closedAnswer;
      this.uploadedFileName = uploadAnswer.fileName;
      this.uploadedFileId = uploadAnswer.fileId;
      this.isFinished = true;
    }
  }

  // uploadedFile: File;
  uploadedFileName: string;
  uploadedFileId: number;

  public dropped(event: UploadEvent) {
    // console.log('dropped');
    // this.onChange(true);
    let uploadFile: UploadFile = event.files[0];
    let fileEntry = uploadFile.fileEntry;
    fileEntry.file((file: File) => {
      this.onSubmit(file);
      console.log("this is being executed");
    });
    // this.onSubmit(fileEntry.file)

    // this.onSubmit(event.files[0]);
  }

  public fileOver(event){
    // console.log('fileOver');
  }

  public fileLeave(event){
    // console.log('fileLeave');
  }

  fileChange(event){
    // console.log("change in fileupload");
    // this.onChange(true);
    this.onSubmit(event.target.files.item(0));
  }

  onSubmit(file: File){
    // let file: File = event.target.files.item(0);

    let fileId: number;

    this.upload.uploadFile(file).subscribe(
      data => {
        this.uploadedFileId = data.fileId;
        let result = {} as UploadAnswer;
        this.route.params.subscribe(
          params => result =
            {
              questionnaireInstanceId: params.id,
              questionId: this.question.id,
              type: "UPLOAD",
              fileId: this.uploadedFileId,
              fileName: file.name
            }
        );
        this.uploadedFileName = file.name;
        console.log("value of uploadfile: "+this.uploadedFileName);
        this.onChange({isFinished: true, data: [result]});
        console.log("value of isFinished: "+this.isFinished);
        // this.cdRef.detectChanges();
      },
        err => {this.toastService.setMessage(err.error.message)}
    );
  }

  downloadFile(fileId: number, filename: string){
    this.upload.downloadFile(fileId, filename);
  }
}
