import {
  AfterContentChecked, AfterContentInit, AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, Input,
  OnChanges,
  OnInit
} from '@angular/core';
import {BaseComponent, Interceptor} from "../base/base.component";
import {NgbPopoverConfig} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {ClosedAnswer, OpenAnswer} from "../../../services/questionnaire/answer/answer-service.service";

@Component({
  selector: 'app-questionnaire-closed',
  templateUrl: './closed.component.html',
  providers: [NgbPopoverConfig],
  styleUrls: ['./closed.component.css']
})
export class ClosedComponent extends BaseComponent implements OnInit, Interceptor {


  ngOnInit() {
    if(this.getAnswer()) {
      let answer = this.getAnswer() as ClosedAnswer;
      for (let option of this.question.options) {
        if (option.id == answer.optionId) {
          // this.setFollowUpQuestion(option);
          this.selectedOption = option;
          this.followUpQuestion = option.followUpQuestion;
          this.isFinished = true;
          break;
        }
      }
    }
  }

  //
  // ngAfterContentChecked(): void {
  //   // console.log("on changes");
  //   // if(this.getAnswer()){
  //   //   console.log("viewchecked: Chosen option for this questionId "+ this.question.id+ " is: "+JSON.stringify(this.container.getAnswers()[this.question.id]));
  //     let answer = this.getAnswer() as ClosedAnswer;
  //     for(let option of this.question.options){
  //       if(option.id == answer.optionId){
  //         this.setFollowUpQuestion(option);
  //         break;
  //       }
  //     }
  //   }
  // }



  followUpQuestion;

  selectedOption;

  constructor(config: NgbPopoverConfig,
              private route: ActivatedRoute,
              cdRef:ChangeDetectorRef) {
    super(cdRef);
    config.placement = 'left';
    this.interceptor = this;
  }

  setFollowUpQuestion(option) {
    this.selectedOption = option;
    this.followUpQuestion = option.followUpQuestion;
    console.log('setting subquestion: ' + this.followUpQuestion);



    if (this.followUpQuestion === undefined || this.followUpQuestion == null) {
      // console.log('emitting');
      // this.finishEmitter.emit(true);

      this.onChange({isFinished: true, data: []});
      // this.onChange(true);
    }else{
      // this.onChange(false)
      this.onChange({isFinished: false, data: []});
    }
    // this.followUpQuestion = followUpQuestion;
  }

  intercept(result) {
    if(result.isFinished) {
      result.data = [this.getClosedAnswer()].concat(result.data)
    }
  }

  getClosedAnswer():ClosedAnswer {
    let result = {} as ClosedAnswer;
    this.route.params.subscribe(
      params => result =
        {
          questionnaireInstanceId: params.id,
          questionId: this.question.id,
          type: "CLOSED",
          optionId: this.selectedOption.id
        }
    );
    console.log("Value of result of closedQuestion: "+JSON.stringify(result));
    return result;
  }




  /**
   * Sets the css classes of a given radio button
   * @param q the option of the question
   * @returns {{radioDesired: any; radioUndesired: boolean}}
   */
  setRadioClasses(option) {
    return {
      radioDesired: option.desired,
      radioUndesired: !option.desired
    };
  }
}
