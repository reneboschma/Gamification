import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SideBarContentComponentComponent } from './side-bar-content-component.component';

describe('SideBarContentComponentComponent', () => {
  let component: SideBarContentComponentComponent;
  let fixture: ComponentFixture<SideBarContentComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SideBarContentComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SideBarContentComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
