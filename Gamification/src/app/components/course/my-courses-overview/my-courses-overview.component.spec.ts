import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCoursesOverviewComponent } from './my-courses-overview.component';

describe('MyCoursesOverviewComponent', () => {
  let component: MyCoursesOverviewComponent;
  let fixture: ComponentFixture<MyCoursesOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCoursesOverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCoursesOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
