import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from "@angular/material";
import {MatTableModule} from '@angular/material/table';
import {CourseServiceService} from "../../../services/course/course-service.service";
import {ToastService} from "../../../services/util/toast/toast.service";

@Component({
  selector: 'app-my-courses-overview',
  templateUrl: './my-courses-overview.component.html',
  styleUrls: ['./my-courses-overview.component.css']
})
export class MyCoursesOverviewComponent implements OnInit{


  constructor(private courseService: CourseServiceService, private toastService: ToastService) { }

  ngOnInit(): void {

  }

  // ngOnInit() {
  //   this.courseService.getOwnCourses().subscribe(
  //     data => {
  //         this.dataSource = new MatTableDataSource(data)
  //       },
  //     err => {
  //         this.toastService.setMessage(err.error.message, 'danger');
  //     });
  //   this.dataSource = new MatTableDataSource()
  // }

}

