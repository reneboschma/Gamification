import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTypePageComponent } from './test-type-page.component';

describe('TestTypePageComponent', () => {
  let component: TestTypePageComponent;
  let fixture: ComponentFixture<TestTypePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestTypePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTypePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
