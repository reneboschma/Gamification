import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-type-add-page',
  templateUrl: './test-type-add-page.component.html',
  styleUrls: ['./test-type-add-page.component.css']
})
export class TestTypeAddPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  shortname: string;
  description: string;
}
