import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestTypeAddPageComponent } from './test-type-add-page.component';

describe('TestTypeAddPageComponent', () => {
  let component: TestTypeAddPageComponent;
  let fixture: ComponentFixture<TestTypeAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestTypeAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestTypeAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
