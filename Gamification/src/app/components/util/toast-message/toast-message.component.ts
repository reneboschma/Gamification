import {Component, OnDestroy, OnInit} from '@angular/core';
import {Message, ToastService} from "../../../services/util/toast/toast.service";
import {Subject} from "rxjs/Subject";
import {Subscription} from "rxjs/Subscription";
import {debounceTime} from 'rxjs/operator/debounceTime';

@Component({
  selector: 'app-toast-message',
  templateUrl: './toast-message.component.html',
  styleUrls: ['./toast-message.component.css']
})
export class ToastMessageComponent implements OnInit, OnDestroy {


  constructor(private toastService: ToastService) { }

  messageSubscription: Subscription;
  messageSubject: Subject<Message>;
  message: Message;

  ngOnInit() {
    this.messageSubject = this.toastService.getMessage();
    this.messageSubscription = this.messageSubject.subscribe(m => {this.message = m});
    debounceTime.call(this.messageSubject, 5000).subscribe(() => this.removeToast());
  }

  removeToast(){
    this.toastService.removeMessage();
  }

  ngOnDestroy(): void {
    this.messageSubscription.unsubscribe();
  }
}
