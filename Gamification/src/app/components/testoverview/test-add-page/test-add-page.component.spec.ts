import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestAddPageComponent } from './test-add-page.component';

describe('TestAddPageComponent', () => {
  let component: TestAddPageComponent;
  let fixture: ComponentFixture<TestAddPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestAddPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
