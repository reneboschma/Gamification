import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {QuestionnaireRetrieverService} from "../../../services/questionnaire/questionnaire-retriever.service";

@Component({
  selector: 'app-my-tests-page',
  templateUrl: './my-tests-page.component.html',
  styleUrls: ['./my-tests-page.component.css'],
  providers:[QuestionnaireRetrieverService]
})
export class MyTestsPageComponent implements OnInit {


  constructor(private questionnaireRetriever:QuestionnaireRetrieverService) { }

  ngOnInit() {
    console.log("Test page init");
    this.questionnaireRetriever.getAvailableQuestionnaires().subscribe(data => console.log("data: "+data))
  }

}
