import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestAddFormComponent } from './test-add-form.component';

describe('TestAddFormComponent', () => {
  let component: TestAddFormComponent;
  let fixture: ComponentFixture<TestAddFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestAddFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestAddFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
