import { Component, OnInit } from '@angular/core';
import {TestType, TestTypeServiceService} from "../../../services/questionnaire/testtype/test-type-service.service";
import {ErrorMessageService} from "../../../services/util/error/error-message.service";
import {NgbPopoverConfig} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {Test, TestServiceService} from "../../../services/test/test-service.service";
import {ToastService} from "../../../services/util/toast/toast.service";
import {UserInformation, UserInformationService} from "../../../services/util/userinformation/user-information.service";

@Component({
  selector: 'app-test-add-form',
  templateUrl: './test-add-form.component.html',
  styleUrls: ['./test-add-form.component.css'],
  providers: [TestTypeServiceService, TestServiceService]
})
export class TestAddFormComponent implements OnInit {

  constructor(private testTypeService: TestTypeServiceService,
              private errorMessageService: ErrorMessageService,
              private config: NgbPopoverConfig,
              private testService: TestServiceService,
              private toastService: ToastService,
              private userInformationService: UserInformationService) {
    // config.placement = 'right';
  }

  ngOnInit() {
    let email = this.userInformationService.getUserInformation().mainEmail;
    this.teachersToFillInIds.push(email);
    this.mainResponsibleId = email;

    this.testTypeService.getTestTypes().subscribe(
      data => {
        this.testTypeOptions = data;
        this.testType = data[0];
      },
      err => this.errorMessageService.openErrorDefault(err.error.message)
    );
  }

  courseId: number;
  testName: string;
  testType: TestType;
  date: Date;
  mainResponsibleId: string;
  // firstToFillIn: string = "reneboschma@hotmail.com";
  teachersToFillInIds: string[] = [];

  testTypeOptions: TestType[];


  removeEmail(email){
    // console.log(JSON.stringify(email));
    let index = this.teachersToFillInIds.indexOf(email);
    this.teachersToFillInIds.splice(index,1);
  }

  addEmail(email){
    this.teachersToFillInIds.push(email.value);
    email.value = "";
  }

  submitTest(){
    let test = {} as Test;
    test.courseId = this.courseId;
    test.testName = this.testName;
    test.mainResponsibleId = this.mainResponsibleId;
    test.teachersToFillInIds = this.teachersToFillInIds;
    test.testTypeId = this.testType.id;
    test.date = this.date;

    this.testService.addTest(test).subscribe(
      oke => {this.toastService.setMessage("Successfully submitted the test.", "success")},
      err => this.toastService.setMessage(err.error.message, "danger")
    );

  }
}
