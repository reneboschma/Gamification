import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-questionnaire-page',
  templateUrl: './questionnaire-page.component.html',
  styleUrls: ['./questionnaire-page.component.css']
})
export class QuestionnairePageComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  questionnaireInstanceId;

  ngOnInit() {
    this.route.params.subscribe(params => this.questionnaireInstanceId = params.id);

  }

}
