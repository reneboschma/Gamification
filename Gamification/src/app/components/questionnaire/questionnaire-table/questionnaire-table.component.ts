import { Component, OnInit } from '@angular/core';
import {Sort} from "@angular/material";
import {ActivatedRoute, Router} from "@angular/router";
import {QuestionnaireRetrieverService} from "../../../services/questionnaire/questionnaire-retriever.service";

@Component({
  selector: 'app-questionnaire-table',
  templateUrl: './questionnaire-table.component.html',
  styleUrls: ['./questionnaire-table.component.css']
})
export class QuestionnaireTableComponent implements OnInit {

  // questionnaireEntries = [
  //   {id: 1, courseName: "CourseName", testName: "TestName", testDate: new Date(2018, 2, 28)},
  //   {id: 2, courseName: "CourseName2", testName: "TestName2", testDate: new Date(2018, 2, 28)},
  //   {id: 3, courseName: "CourseName3", testName: "TestName3", testDate: new Date(2018, 2, 27)},
  // ];

  questionnaireEntries;

  sortedData;

  constructor(private router: Router, private questionnaireRetriever: QuestionnaireRetrieverService) {

  }

  ngOnInit() {
    this.questionnaireRetriever.getAvailableQuestionnaires().subscribe(
      data => {
        this.questionnaireEntries = data;
        this.sortedData = this.questionnaireEntries.slice();
      }
    );

  }

  sortData(sort: Sort) {
    const data = this.questionnaireEntries.slice();
    if (!sort.active || sort.direction == '') {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      let isAsc = sort.direction == 'asc';
      switch (sort.active) {
        case 'courseName': return compare(a.courseName, b.courseName, isAsc);
        case 'testName': return compare(a.testName, b.testName, isAsc);
        case 'testDate': return compare(a.testDate, b.testDate, isAsc);
        default: return 0;
      }
    });
  }

  gotoQuestionnaire(questionnaireInstanceId){
    this.router.navigate(['mytests', 'fillin', questionnaireInstanceId]);
  }
}

function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}


