import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { FileDropModule } from 'ngx-file-drop';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { OpenComponent } from './questionnaire/question-entries/open/open.component';
import { BaseComponent } from './questionnaire/question-entries/base/base.component';
import { EntryComponent } from './questionnaire/question-entries/entry/entry.component';
import { ContainerComponent } from './questionnaire/container/container.component';
import { ClosedComponent } from './questionnaire/question-entries/closed/closed.component';
import { UploadComponent } from './questionnaire/question-entries/upload/upload.component';
import { ParentEntryComponent } from './questionnaire/question-entries/parent-entry/parent-entry.component';

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ApiInterceptor} from "./services/util/apiinterceptor.service";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import {MatToolbarModule} from '@angular/material/toolbar';
import { MainComponentComponent } from './components/main/main-component/main-component.component';
import {
  MatButtonModule,
  MatDividerModule, MatFormFieldModule,
  MatGridListModule, MatIconModule, MatInputModule, MatListModule, MatMenuModule, MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule, MatSnackBarModule, MatSortModule, MatTableModule
} from "@angular/material";
import { ToolBarComponentComponent } from './components/main/tool-bar-component/tool-bar-component.component';
import { SideBarContentComponentComponent } from './components/main/side-bar-content-component/side-bar-content-component.component';
import {RouterModule, Routes} from "@angular/router";
import { PageNotFoundComponent } from './components/main/page-not-found/page-not-found.component';
import { HomeComponent } from './components/main/home/home.component';
import { MyTestsPageComponent } from './components/testoverview/my-tests-page/my-tests-page.component';
import { ApiTesterComponent } from './components/main/api-tester/api-tester.component';
import { QuestionnaireTableComponent } from './components/questionnaire/questionnaire-table/questionnaire-table.component';
import { QuestionnairePageComponent } from './components/questionnaire/questionnaire-page/questionnaire-page.component';
import {ErrorMessageService} from "./services/util/error/error-message.service";
import { TestAddPageComponent } from './components/testoverview/test-add-page/test-add-page.component';
import { TestAddFormComponent } from './components/testoverview/test-add-form/test-add-form.component';
import { ToastMessageComponent } from './components/util/toast-message/toast-message.component';
import {ToastService} from "./services/util/toast/toast.service";
import {UserInformationService} from "./services/util/userinformation/user-information.service";
import { TestTypePageComponent } from './components/testtype/test-type-page/test-type-page.component';
import { TestTypeAddPageComponent } from './components/testtype/test-type-add-page/test-type-add-page.component';
import { ThankYouPageComponent } from './components/questionnaire/thank/thank-you-page/thank-you-page.component';
import { MyCoursesOverviewComponent } from './components/course/my-courses-overview/my-courses-overview.component';



const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'mytests', component: MyTestsPageComponent },
  { path: 'mycourses', component: MyCoursesOverviewComponent },
  { path: 'test/add', component: TestAddPageComponent },
  { path: 'testtype', component: TestTypePageComponent },
  { path: 'testtype/add', component: TestTypeAddPageComponent },
  { path: 'thankyou', component: ThankYouPageComponent },
  { path: 'apitester', component: ApiTesterComponent },
  { path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: 'mytests/fillin/:id', component: QuestionnairePageComponent},
  { path: '**', component: PageNotFoundComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    OpenComponent,
    BaseComponent,
    EntryComponent,
    ContainerComponent,
    ClosedComponent,
    UploadComponent,
    ParentEntryComponent,
    MainComponentComponent,
    ToolBarComponentComponent,
    SideBarContentComponentComponent,
    PageNotFoundComponent,
    HomeComponent,
    MyTestsPageComponent,
    ApiTesterComponent,
    QuestionnaireTableComponent,
    QuestionnairePageComponent,
    TestAddPageComponent,
    TestAddFormComponent,
    ToastMessageComponent,
    TestTypePageComponent,
    TestTypeAddPageComponent,
    ThankYouPageComponent,
    MyCoursesOverviewComponent
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FileDropModule,
    FormsModule,
    AngularFontAwesomeModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatTableModule,
    MatProgressSpinnerModule,
    MatSidenavModule,
    MatGridListModule,
    MatListModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    MatDividerModule,
    MatSortModule,
    MatSnackBarModule,
    MatButtonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: ApiInterceptor,
    multi: true,
  },
    ErrorMessageService,
    ToastService,
    UserInformationService,
    { provide: APP_INITIALIZER, useFactory: userInfromationProviderFactory, deps: [UserInformationService], multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function userInfromationProviderFactory(service: UserInformationService) {
  return () => service.load();
}
