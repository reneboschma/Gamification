import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";


export interface Answer {
  questionnaireInstanceId: number,
  questionId: number,
  type:string
}

export interface OpenAnswer extends Answer {
  content: string;
}

export interface ClosedAnswer extends Answer {
  optionId: number;
}

export interface UploadAnswer extends Answer {
  fileId: number,
  fileName: string;
}

@Injectable()
export class AnswerServiceService {

  constructor(private http:HttpClient) { }



  postAnswer(data){
    return this.http.post("answer/", data)
  }

  getAnswers(questionnaireId:number): Observable<Answer[]>{
    return this.http.get<Answer[]>("answer/"+questionnaireId);
  }

}
