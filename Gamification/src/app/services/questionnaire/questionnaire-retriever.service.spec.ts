import { TestBed, inject } from '@angular/core/testing';

import { QuestionnaireRetrieverService } from './questionnaire-retriever.service';

describe('QuestionnaireRetrieverService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestionnaireRetrieverService]
    });
  });

  it('should be created', inject([QuestionnaireRetrieverService], (service: QuestionnaireRetrieverService) => {
    expect(service).toBeTruthy();
  }));
});
