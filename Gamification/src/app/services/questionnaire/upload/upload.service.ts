import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import { saveAs } from 'file-saver/FileSaver';

export interface FileResponse {
  fileId: number
}

@Injectable()
export class UploadService {

  constructor(private http: HttpClient) { }


  uploadFile(file: File): Observable<FileResponse>{
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this.http.post<FileResponse>("upload/", formData);
  }

  downloadFile(fileId: number, filename:string) {
    console.log("file to download: "+filename);
    return this.http.get("download/"+fileId, { responseType: 'blob' })
      .subscribe(response => {
        let blob = new Blob([response], {
          type: "application/octet-stream"
        });
        saveAs(blob, filename);
      });
  }


}
