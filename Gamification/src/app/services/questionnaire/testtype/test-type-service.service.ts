import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

export interface TestType {
  id: number;
  shortname: string;
  description: string;
}

@Injectable()
export class TestTypeServiceService {

  constructor(private http: HttpClient) { }


  getTestTypes(): Observable<TestType[]> {
    return this.http.get<TestType[]>("testtype/");
  }
}
