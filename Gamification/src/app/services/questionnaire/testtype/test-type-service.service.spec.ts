import { TestBed, inject } from '@angular/core/testing';

import { TestTypeServiceService } from './test-type-service.service';

describe('TestTypeServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TestTypeServiceService]
    });
  });

  it('should be created', inject([TestTypeServiceService], (service: TestTypeServiceService) => {
    expect(service).toBeTruthy();
  }));
});
