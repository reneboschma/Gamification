import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

export interface QuestionnaireInstanceEntry {
  id: number,
  testName: string,
  courseName: string,
  testDate: Date
}
@Injectable()
export class QuestionnaireRetrieverService {

  constructor(private http:HttpClient) { }

  getAvailableQuestionnaires(): Observable<QuestionnaireInstanceEntry[]> {
    return this.http.get<QuestionnaireInstanceEntry[]>("questionnaire/list");
  }

  getQuestionnaireFromQuestionnaireInstance(questionnaireInstaceId): Observable<any>{
    // return this.http.post("questionnaire/getQuestionnaire", {id: questionnaireInstaceId});
    return this.http.get<any>("questionnaire/"+questionnaireInstaceId);
    // return this.http.post("questionnaire/getQuestionnaire", {});
    // return this.http.post<any>("questionnaire/getQuestionnaire", {});
  }

}
