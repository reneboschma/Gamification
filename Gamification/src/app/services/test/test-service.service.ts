import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

export interface Test {
  courseId: number;
  testName: string;
  testTypeId: number;
  date: Date;
  mainResponsibleId: string;
  teachersToFillInIds: string[];
}

@Injectable()
export class TestServiceService {

  constructor(private http:HttpClient) { }

  addTest(test: Test): Observable<any>{
    return this.http.post("test/", test);
  }

}
