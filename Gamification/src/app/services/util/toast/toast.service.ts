import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";


// style: success for green, danger for red
export class Message {
  content: string;
  style: string;
  dismissed: boolean = false;

  constructor(content, style?) {
    this.content = content;
    this.style = style || 'info';
  }
}

@Injectable()
export class ToastService {

  constructor() { }

  message: Subject<Message> = new Subject<Message>();

  getMessage(): Subject<Message>{
    return this.message;
  }

  setMessage(content: string, style?: string) {
    const m = new Message(content, style);
    this.message.next(m);
  }

  removeMessage() {
    this.message.next();
  }

}
