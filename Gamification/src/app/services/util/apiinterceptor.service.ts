import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {environment} from '../../../environments/environment';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {


  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const apiReq = req.clone({url: environment.defaultUrl + `/rest/${req.url}`, withCredentials: true});


    // apiReq.headers.append("JSONID",  "76ED4CC5AED4B5404BE0AE29C4BE1FBD");
    return next.handle(apiReq);
  }
}
