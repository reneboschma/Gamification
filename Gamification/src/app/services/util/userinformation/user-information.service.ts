import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ToastService} from "../toast/toast.service";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";


export interface UserInformation {
  mainEmail: string,
  secondaryEmails: string[],
  id: string,
  permissions: permissions;
}

export interface permissions {
  file: boolean,
  permission: boolean,
  test: boolean,
  testtype: boolean,
  activated: boolean,
  questionnaire: boolean,
  course: boolean
}
@Injectable()
export class UserInformationService {

  userInformation: UserInformation;

  constructor(private http: HttpClient, private toastService: ToastService) {
    // this.retrieveUserInformation();
  }

  load(){
    let promise = new Promise<UserInformation>((resolve, reject) => {
      if(this.userInformation == undefined){
        this.http.get<UserInformation>("teacher/").subscribe(
            res => {
              this.userInformation = res;
              resolve();
            }
          )
        }
      }
    );
    return promise;
      // this.http.get<UserInformation>("user/").subscribe(
      //   data => {this.userInformation.next(data)},
      //   err => this.toastService.setMessage("Could not retrieve user information.")
      // );

  }

  // private setUserInformation(information: UserInformation){
  //   this.userInformation = information;
  // }

  getUserInformation(): UserInformation{
    // if (this.userInformation == null || this.userInformation == undefined){
    //   this.retrieveUserInformation();
    // }
    return this.userInformation;
  }

}
