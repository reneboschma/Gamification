import {inject, TestBed} from '@angular/core/testing';

import {ApiInterceptor} from './apiinterceptor.service';

describe('ApiinterceptorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApiInterceptor]
    });
  });

  it('should be created', inject([ApiInterceptor], (service: ApiInterceptor) => {
    expect(service).toBeTruthy();
  }));
});
