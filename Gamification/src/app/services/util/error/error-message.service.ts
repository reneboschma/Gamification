import { Injectable } from '@angular/core';
import {MatSnackBar} from "@angular/material";

@Injectable()
export class ErrorMessageService {

  constructor(public snackBar: MatSnackBar) { }

  public openError(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 5000,
    });
  }

  public openErrorDefault(message){
    this.openError(message, "hide");
  }

}
