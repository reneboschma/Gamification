import { Injectable } from '@angular/core';
import {ToastService} from "../util/toast/toast.service";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";

export interface CourseOverview {
  courseCode: string,
  courseName: string,
  id: number,
  year: number
}

@Injectable()
export class CourseServiceService {

  constructor(private http: HttpClient) { }


  getOwnCourses(): Observable<CourseOverview[]> {
    return this.http.get<CourseOverview[]>("course/own");
  }

  getAllCourses(): Observable<CourseOverview[]> {
    return this.http.get<CourseOverview[]>("course/all");
  }
}
