import { AppPage } from './app.po';
import {browser, by, element, protractor} from "protractor";

describe('gamification App', () => {
  browser.waitForAngularEnabled(false);
  browser.get("/");
  element(by.id('okta-signin-username')).sendKeys("reneboschma@gmail.com");
  element(by.id('okta-signin-password')).sendKeys("Testaccount1");
  element(by.id('okta-signin-submit')).click().then(function(){
    return browser.driver.wait(function() {
      return browser.driver.getCurrentUrl().then(function(url) {
        return /home/.test(url);
      });
    }, 100000);
  });


  it('should have a title', function() {
    expect(browser.getTitle()).toEqual('Gamification');
  });

});
